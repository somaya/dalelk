<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('details')->nullable();
            $table->string('payment_type');
            $table->string('delivary_point_latitude')->nullable();
            $table->string('delivary_point_langitude')->nullable();
            $table->string('delivary_address')->nullable();
            $table->string('delivary_time')->nullable();
            $table->string('order_number');
            $table->string('status');
            $table->float('cost')->nullable();

            $table->unsignedBigInteger('store_id')->nullable();
            $table->foreign('store_id')
                ->references('id')->on('stores')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedBigInteger('coupon_id')->nullable();
            $table->foreign('coupon_id')
                ->references('id')->on('coupons')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_orders');
    }
}
