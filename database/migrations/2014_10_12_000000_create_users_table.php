<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('phone');
            $table->string('photo')->nullable();
            $table->string('latitude')->nullable();
            $table->string('langitude')->nullable();
            $table->integer('birthyear')->nullable();
            $table->string('login_code')->nullable();
            $table->timestamp('code_expire_at')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('type')->default(1); // 1 للمستخدم و2 للمندوب
            $table->tinyInteger('role')->default(2); // 1 for admin ,2 for user
            $table->tinyInteger('notifiable')->default(1);
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('tokens')->nullable();
            $table->string('device_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
