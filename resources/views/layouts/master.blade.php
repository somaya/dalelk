<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{setting('site_description')}}" />
    <meta name="keywords" content="{{setting('site_tagged')}}" />
    <meta name="author" content="mohamed maree m7mdmaree26@gmail.com" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="{{asset('dashboard/uploads/setting/site_logo/'.setting('site_logo'))}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
{{--    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <title>{{ setting('site_title') }}</title>
    @yield('style')
    <style>

        .up-label .tit{
            font-weight: bold;
            color: {{setting('base_color')}};
        }
        .form .btn,
        .wpwl-button-pay{
            background: {{setting('base_color')}};
        }
        .app-title{
            color: {{setting('base_color')}};
        }
        .wpwl-button-pay {
            display: block;
            margin: auto;
            float: unset;
            width: 100%;
        }

        .wpwl-form {
            background: transparent;
            border: none;
            box-shadow: none;
            padding: 10px 0;
        }
        @media (min-width: 480px) {
            .wpwl-form-card .wpwl-group-cardNumber, .wpwl-form-card .wpwl-group-cardHolder, .wpwl-form-card .wpwl-group-birthDate, .wpwl-form-card .wpwl-group-brand-v2 ,
            .wpwl-form-card .wpwl-group-expiry, .wpwl-group-brandLogo, .wpwl-form-card .wpwl-group-cvv{
                padding-left:0;
                width: 100%;
            }
        }
        .wpwl-label-brand, .wpwl-wrapper-brand{
            padding-left: 0;
        }
        .wpwl-group, .wpwl-label-brand, .wpwl-label-brand-v2, .wpwl-wrapper-brand, .wpwl-wrapper-brand-v2, .wpwl-wrapper-registration{
            float: unset;
        }
        .wpwl-brand-card {
            float: unset;
            position: absolute;
            top: 0;
            left: 0;
        }
        .wpwl-button-pay,
        .form .form-control:focus{
            border-color: {{setting('base_color')}};
        }
        .form .btn-send:hover,
        .wpwl-button-pay:hover{
            background: #fff;
            border-color: {{setting('base_color')}};
            color:{{setting('base_color')}};
        }
        iframe[name='card.cvv'],iframe[name='card.number']{
            direction: ltr;
        }
        .wpwl-label-brand,
        .wpwl-label{
            font-weight: bold;
            margin-bottom: 7px;
        }
        .a-s{
            background: #fff;
            padding: 10px;
            border-radius: 5px;
            font-weight: bold;
            text-align: center;
        }
    </style>
</head>
<body>

    <!-- Start Header -->
{{--    <h4 style="font-weight: bold;font-size: 12px;padding:15px">نموذج توثيق</h4>--}}
    <header>
        <div class="logo">
            <img src="{{asset('/'.$settings->logo)}}" />
        </div>
    </header>

    <!-- End Header -->

    <!-- Start Content -->

    <section>
        <div class="content-login">
            <div class="container">
                <div class="text-center">
                    <h4 style="font-size: 15px;margin-bottom: 10px">نموذج توثيق مندوب في تطبيق </h4>
                    <h3 class="app-title">{{setting('site_title')}}</h3>
                </div>

            @if (session('successmsg'))
                <div class="text-success a-s" role="alert">
                {{ session('successmsg') }}
                </div>
            @elseif(session('msg'))
                <div class="text-danger a-s" role="alert">
                {{ session('msg') }}
                </div>
            @endif
                @yield('content')

            </div>
        </div>
    </section>
    <img class="fixed-img" src="http://marsol-aait.4hoste.com/dashboard/images/undraw_walk_in_the_city_1ma6.png" alt="">

    <!-- End Content -->
    <script src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
{{--    <script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
// Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
// Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    @yield('script')
</body>
</html>
