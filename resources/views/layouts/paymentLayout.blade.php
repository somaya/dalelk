<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{setting('site_description')}}" />
    <meta name="keywords" content="{{setting('site_tagged')}}" />
    <meta name="author" content="mohamed maree m7mdmaree26@gmail.com" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="{{asset('dashboard/uploads/setting/site_logo/'.setting('site_logo'))}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <script src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
    <title>{{ setting('site_title') }}</title>
    <style>
        .up-label .tit{
            font-weight: bold;
            color: {{setting('base_color')}};
        }
        .form .btn,
        .wpwl-button-pay{
            background: {{setting('base_color')}};
        }
        .app-title{
            color: {{setting('base_color')}};
        }
        .wpwl-button-pay {
            display: block;
            margin: auto;
            float: unset;
            width: 100%;
        }

        .wpwl-form {
            background: transparent;
            border: none;
            box-shadow: none;
            padding: 10px 0;
        }
        @media (min-width: 480px) {
            .wpwl-form-card .wpwl-group-cardNumber, .wpwl-form-card .wpwl-group-cardHolder, .wpwl-form-card .wpwl-group-birthDate, .wpwl-form-card .wpwl-group-brand-v2 ,
            .wpwl-form-card .wpwl-group-expiry, .wpwl-group-brandLogo, .wpwl-form-card .wpwl-group-cvv{
                padding-left:0;
                width: 100%;
            }
        }
        .wpwl-label-brand, .wpwl-wrapper-brand{
            padding-left: 0;
        }
        .wpwl-group, .wpwl-label-brand, .wpwl-label-brand-v2, .wpwl-wrapper-brand, .wpwl-wrapper-brand-v2, .wpwl-wrapper-registration{
            float: unset;
        }
        .wpwl-brand-card {
            float: unset;
            position: absolute;
            top: 0;
            left: 0;
        }
        .wpwl-button-pay,
        .form .form-control:focus{
            border-color: {{setting('base_color')}};
        }
        .form .btn-send:hover,
        .wpwl-button-pay:hover{
            background: #fff;
            border-color: {{setting('base_color')}};
            color:{{setting('base_color')}};
        }
        iframe[name='card.cvv'],iframe[name='card.number']{
            direction: ltr;
        }
        .wpwl-label-brand,
        .wpwl-label{
            font-weight: bold;
            margin-bottom: 7px;
        }
        .a-s{
            background: #fff;
            padding: 10px;
            border-radius: 5px;
            font-weight: bold;
            text-align: center;
        }
        /*body{*/
        /*    min-height: 100vh;*/
        /*}*/
    </style>
</head>
<body>
    
    <!-- Start Header -->
{{--    <h4 style="font-weight: bold;font-size: 12px;padding: 15px">@yield('head')</h4>--}}
    <header>
        <div class="logo">
            <img src="{{asset('dashboard/uploads/setting/site_logo/'.setting('site_logo'))}}" />
        </div>
    </header>

    <!-- End Header -->

    <!-- Start Content -->

    <section>
        <div class="content-login">
            <div class="container">
               <div class="text-center">
                   <h4>@yield('head')</h4>
                   <h3 class="app-title">{{setting('site_title')}}</h3>
               </div>
            @if (session('successmsg'))
                <div class="text-success a-s" role="alert">
                {{ session('successmsg') }}
                </div>
            @elseif(session('msg'))
                <div class=" text-danger a-s" role="alert">
                {{ session('msg') }}
                </div>            
            @endif          
                @yield('content')

            </div>
        </div>
    </section>
    <img class="fixed-img" src="http://marsol-aait.4hoste.com/dashboard/images/undraw_walk_in_the_city_1ma6.png" alt="">

    <!-- End Content -->

    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>

</body>
</html>
