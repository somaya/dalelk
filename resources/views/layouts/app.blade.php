<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{setting('site_description')}}" />
    <meta name="keywords" content="{{setting('site_tagged')}}" />
    <meta name="author" content="mohamed maree m7mdmaree26@gmail.com" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="{{url('img/logo.png')}}">

    <script src="{{asset('js/jquery-1.11.2.min.js')}}"></script>
    <title>{{ setting('site_title') }}</title>

</head>
<body>



                @yield('content')



</body>
</html>
