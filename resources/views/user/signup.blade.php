@extends('layouts.master')
@section('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <!-- Islamic DataPiker -->

    {{--    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />--}}

    <link href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css" rel="stylesheet">
    <style>
        .main-input .invalid-feedback {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);

        }

        .card-header {
            padding: 0
        }

        .card-header button {
            margin-bottom: 0
        }

        .select2-container--default .select2-selection--single {
            /*border-radius: 30px;*/
            height: 40px;
            line-height: 40px;
            border: 1px solid #dadada;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 40px;
        }

        .select2-container {
            width: 100% !important;
            direction: rtl;
        }

        .notsaudi {
            display: none;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            right: unset;
            left: 3px;
            height: 40px;
        }

        .w3-card-4 {
            left: 0 !important;
            right: 0;
            margin: auto !important;
            z-index: 9 !important;
        }

        .main-title {
            background: #f3f3f3;
            padding: 10px;
            font-size: 17px;
            font-weight: bold;
        }

        .cus-p {
            margin-bottom: 10px;
            font-size: 14px;
        }


        .main-input {
            position: relative;
            line-height: 40px;
            padding: 0 15px 0 0;
            border: 1px solid #C6C6C6;
            border-radius: 5px;
            height: 40px;
        }

        .main-input .file-input {
            position: absolute;
            opacity: 0;
            width: 100%;
            right: 0;
            height: 100%;
        }

        .main-input i {
            position: absolute;
            width: 40px;
            text-align: center;
            height: 40px;
            line-height: 40px;
            z-index: 3;
            left: 5px;
            right: auto;
            font-size: 20px;
        }

        .file-name {
            font-size: 12px;
            color: #928d8d;
        }

        .form label {
            font-size: 13px;
        }

        .main-input .uploaded-image {
            position: absolute;
            top: 50%;
            height: auto;
            left: 0;
            z-index: 9;
            transform: translate(0, -50%);
            cursor: pointer;
            transition: all .3s;
        }

        .main-input .uploaded-image img {
            max-height: 40px;
            min-width: 40px;
            display: block;
            border-radius: 8px;
            transition: all .3s;
            height: 100%;
            max-width: 100% !important;
        }

        .main-input .uploaded-image.active img {
            max-height: 400px;
            min-width: auto;
            max-width: calc(100vw - 20px);
            height: auto;
        }

        .main-input .uploaded-image.active {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 999999;
            width: auto;
            height: auto;
            box-shadow: none;
        }

        .orders h4,
        .orders h3 {
            color: blue;
            text-align: right;
            font-size: 17px;
            font-weight: bold;
        }

        .orders ol {
            padding-right: 0;
        }

        .orders p {
            font-size: 14px;
        }

        #accordion button.collapsed {
            background: #f3f3f3;
            color: #000;
        }

        #accordion button {
            width: 100%;
            background: blue;
            text-align: right;
            font-weight: bold;
            color: #fff;
            position: relative;
        }

        #accordion button:hover,
        #accordion button:focus {
            outline: none;
            text-decoration: none;
        }

        #accordion button.collapsed:after {
            content: "\f067";
        }

        #accordion button:after {
            font-family: 'FontAwesome';
            content: "\f068";
            float: left;
        }
    </style>
@endsection
@section('content')

    <form class="form needs-validation" novalidate action="{{route('signupDelegation')}}" id="signupDelegationForm"
          method="POST" enctype="multipart/form-data">
        <h4 class="main-title">ملاحظة مهمة</h4>
        <p class="cus-p">يجب عليك التحقق من ملفك الشخصي الخاص بك من خلال خانة “أنا” في التطبيق:</p>
        <p class="cus-p">1- أن تكون الصورة لك واضحة من غير نظارات شمسية وتظهر كامل الوجه.</p>
        <p class="cus-p">2- الاسم الثلاثي لك كما هو ظاهر بالهوية الوطنية.</p>
        <p class="cus-p">مع العلم أنه لن يتم النظر لأي طلب لا يحقق الشروط المذكورة أعلاه.</p>
        {{csrf_field()}}
        <h4 class="main-title">بيانات المندوب</h4>
        <div class="form-group">
            <label class="label-control">تسجيل الاسم كما هو مسجل في الهوية الشخصية
                <span style="color: blue">*</span>
                full Name</label>
            <input type="text" name="fullname" class="form-control" value="{{($user)? $user->name : ''}}"
                   placeholder="تسجيل الاسم كما هو مسجل في الهوية الشخصية * full Name" required/>
            <div class="invalid-feedback">
                ادخل الاسم كاملا
            </div>
            @if ($errors->has('fullname'))
                <span class="form-control-feedback" role="alert">
                 <small class="text-danger">{{ $errors->first('fullname') }}</small>
            </span>
            @endif
        </div>
        <div class="form-group">
            <label class="label-control">رقم جوال فعال لاستلام طلبات التوصيل عليه
                <span style="color: blue">*</span>
                phone</label>
            <input type="text" name="phone" class="form-control" value="{{($user)? '0'.$user->phone : ''}}"
                   placeholder="رقم الجوال * phone" required/>
            <div class="invalid-feedback">
                ادخل رقم الجوال
            </div>
            @if ($errors->has('phone'))
                <span class="form-control-feedback" role="alert">
                <small class="text-danger">{{ $errors->first('phone') }}</small>
            </span>
            @endif
        </div>
        <div class="form-group">
            <label class="label-control">رقم الهوية الوطنية / هوية مقيم
                <span style="color: blue">*</span>
                NationalID</label>
            <input type="number" name="identity_card" value="{{old('identity_card')}}" class="form-control"
                   placeholder="رقم الهوية الوطنية / هوية مقيم * NationalID" required/>
            <div class="invalid-feedback">
                ادخل رقم الهويه الوطنية /هوية المقيم
            </div>
            @if ($errors->has('identity_card'))
                <span class="form-control-feedback" role="alert">
                <small class="text-danger">{{ $errors->first('identity_card') }}</small>
            </span>
            @endif
        </div>

        <div class="upload">
            <div class="form-group">
                <label class="label-control">صورة الهوية الشخصية
                    <span style="color: blue">*</span>
                    Identity Card image</label>
                <div class="main-input">
                    <i class="fa fa-camera gray"></i>
                    <span class="file-name text-right gray">

                    </span>
                    <input class="form-control file-input" type="file" accept="image/*" name="identity_card_image" required/>
                    <div class="invalid-feedback">
                        ادخل صورة الهوية الشخصية
                    </div>
                    <div class="uploaded-image"></div>
                </div>

            </div>
        </div>

        <div class="form-group">
            <label class="label-control">تاريخ ميلاد السائق
                <span style="color: blue">*</span>
                Driver date of birth</label>
            <input type="text" name="driver_date_of_birth" value="{{old('driver_date_of_birth')}}" id="calender"
                   class="form-control hijri-date-default" placeholder="تاريخ ميلاد السائق  * Driver date of birth"
                   required/>
            <div class="invalid-feedback">
                ادخل تاريخ ميلاد السائق
            </div>
        </div>


        <div class="form-group">
            <label class="label-control">البريد الإلكتروني
                <span style="color: blue">*</span>
                Email</label>
            <input type="email" name="email" value="{{old('email')}}" class="form-control"
                   placeholder="البريد الإلكتروني" required/>
            <div class="invalid-feedback">
                ادخل البريد الالكتروني
            </div>
            @if ($errors->has('email'))
                <span class="form-control-feedback" role="alert">
                <small class="text-danger">{{ $errors->first('email') }}</small>g>
            </span>
            @endif
        </div>


        <div class="form-group">
            <label class="label-control">اسم البنك
                <span style="color: blue">*</span>
                مثل : بنك الراجحي - الاهلي </label>
            <input style="height: 38px" type="text" name="bank"
                   value="{{old('bank')}}" class="form-control"
                   placeholder="ادخل اسم البنك" required/>
            <div class="invalid-feedback">
                ادخل اسم البنك
            </div>
            @if ($errors->has('bank_iban_number'))
                <span class="form-control-feedback" role="alert">
                <small class="text-danger">{{ $errors->first('bank_iban_number') }}</small>
            </span>
            @endif
        </div>


        <div class="form-group">
            <label class="label-control">رقم الحساب البنكي الآيبان والمكون من 24 خانة (حاليا ندعم البنوك التالية:
                الأهلي)
                <span style="color: blue">*</span>
                IBAN 24-digit bank account number (currently we support the following banks: AlAhli)</label>
            <input style="height: 38px" type="text" name="bank_iban_number"
                   value="{{old('bank_iban_number')}}" class="form-control"
                   placeholder="رقم حساب البنك الاهلي * Alahly Bank number" required/>
            <div class="invalid-feedback">
                ادخل رقم الحساب البنكي
            </div>
            @if ($errors->has('bank_iban_number'))
                <span class="form-control-feedback" role="alert">
                <small class="text-danger">{{ $errors->first('bank_iban_number') }}</small>
            </span>
            @endif
        </div>

        <div class="upload">
            <div class="form-group">
                <label class="label-control">الصورة الشخصية
                    <span style="color: blue">*</span>
                    personal image</label>
                <div class="main-input">
                    <i class="fa fa-camera gray"></i>
                    <span class="file-name text-right gray">

                    </span>
                    <input class="form-control file-input" type="file" accept="image/*" name="personal_image" required/>
                    <div class="invalid-feedback">
                        ادخل الصورة الشخصية
                    </div>
                    <div class="uploaded-image"></div>
                </div>

            </div>
        </div>

        <div class="upload">
            <div class="form-group">
                <label class="label-control">صورة رخصة القيادة
                    <span style="color: blue">*</span>
                    driving license</label>
                <div class="main-input">
                    <i class="fa fa-camera gray"></i>
                    <span class="file-name text-right gray">

                    </span>
                    <input class="form-control file-input" type="file" accept="image/*" name="driving_license" required/>
                    <div class="invalid-feedback">
                        صورة رخصة القيادة
                    </div>
                    <div class="uploaded-image"></div>
                </div>

            </div>
        </div>

        <div class="upload">
            <div class="form-group">
                <label class="label-control">صورة لمقدمة السيارة المراد استخدامها في التوصيل
                    <span style="color: blue">*</span>
                    car front</label>
                <div class="main-input">
                    <i class="fa fa-camera gray"></i>
                    <span class="file-name text-right gray">

                    </span>
                    <input class="form-control file-input" type="file" accept="image/*" name="car_front" required/>
                    <div class="invalid-feedback">
                        صورة لمقدمة السيارة
                    </div>
                    <div class="uploaded-image"></div>
                </div>

            </div>
        </div>

        <div class="upload">
            <div class="form-group">
                <label class="label-control">صورة خلفية للسيارة يظهر فيها رقم لوحة تسجيل السيارة بوضوح كبير
                    <span style="color: blue">*</span>
                    car back</label>
                <div class="main-input">
                    <i class="fa fa-camera gray"></i>
                    <span class="file-name text-right gray">

                    </span>
                    <input class="form-control file-input" type="file" accept="image/*" name="car_back" required/>
                    <div class="invalid-feedback">
                        صورة خلفية للسيارة
                    </div>
                    <div class="uploaded-image"></div>
                </div>

            </div>
        </div>



        <div class="form-group m-form__group row">
            <label class="label-control">عنوان صاحب الحساب
                <span style="color: blue">*</span>
                Address
            </label>
            <div class="col-lg-10{{ $errors->has('mandoubaddress') ? ' has-danger' : '' }}">
                <input type="text" value="{{isset($mandoub)? $mandoub->address:''}}" name="mandoubaddress"
                       id="mandoubaddress" class="form-control">

                <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                <div id="mandoub_map" lat="{{isset($mandoub)? $mandoub->latitude:'21.4963767'}}"
                     lng="{{isset($mandoub) ? $mandoub->langitude:'39.2017179'}}"
                     style="width: 100%; height: 400px;"></div>
                <input type="hidden" id="mandoub_latitude" name="latitude"
                       value="{{isset($mandoub)? $mandoub->latitude:'21.4963767'}}"/>
                <input type="hidden" id="mandoub_longitude" name="langitude"
                       value="{{isset($mandoub) ? $mandoub->langitude:'39.2017179'}}"/>
                @if ($errors->has('mandoubaddress'))
                    <span class="form-control-feedback" role="alert">
                    <small class="text-danger">{{ $errors->first('mandoubaddress') }}</small>
            </span>
                @endif
            </div>

        </div>


        <div class="accept">
            <h4 style="color: blue;font-size:14px;font-weight: bold ">إقرار وموافقة</h4>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="reading_terms" id="reading_terms" required>
                    <!-- <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> -->
                    أقر بأنني قد قرأت الشروط المذكورة ووافقت عليها،
                </label>
            </div>
        </div>
        <div class="invalid-feedback">
            وافق ع الشروط
        </div>
        <button type="submit" class="btn btn-send">أرسل الطلب</button>
    </form>








    <datepicker-hijri reference="calender" placement="bottom"></datepicker-hijri>
    <datepicker-hijri reference="calender1" placement="bottom"></datepicker-hijri>
@endsection
@section('script')

    <script type="text/javascript">

        $(document).on('change', '#nationality_id', function (e) {
            if ($(this).val() == 1) {
                $(".notsaudi").css({
                    display: 'none'
                });
            } else {
                $(".notsaudi").css({
                    display: 'block'
                });
            }
        });

        $(document).on('change', '#region_id', function (e) {
            e.preventDefault();

            var id = $(this).val();

            $.ajax({
                url: '/changeRegion/' + id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data.key == 'fail') {
                        alert(data.msg);
                    } else {
                        $('#city_id').html(data.html).fadeIn();
                    }

                }
            });
        });

        $('#signupDelegationForm').on('submit', function () {
            var c = $('#reading_terms').prop('checked');
            if (c == false) {
                alert('يجب عليك الموافقة علي الشروط المذكورة.');
                return false;
            }
        })
    </script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2();
        });
    </script>

    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
    <script src="https://cdn.jsdelivr.net/gh/abublihi/datepicker-hijri@master/build/datepicker-hijri.js"
            type="text/javascript">
    </script>
    <script>
        $(document).on("change", ".file-input", function () {
            let input = $(this),
                uploadedImage = input.siblings(".uploaded-image"),
                placeHolder = input.siblings(".placeholder"),
                fileName = input.parent().find(".file-name"),
                plus = input.siblings("i.fas.fa-camera");
            if (input.val() === "") {
                fileName.empty();
                uploadedImage.empty();
                placeHolder.removeClass("active");
                plus.fadeIn(100);
            } else {
                plus.fadeOut(100);
                fileName.text(input.val().replace("C:\\fakepath\\", ""));
                uploadedImage.empty();
                uploadedImage.append('<img src="' + URL.createObjectURL(event.target.files[0]) + '">');
            }
        });
        $(document).on("click", ".file-name", function () {
            $(this).siblings(".file-input").click();
        });

        $(document).on("click", ".uploaded-image", function () {
            $(this).addClass("active");
        });
        $("body").on("click", function () {
            $('.uploaded-image').removeClass("active");
        });
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaFbSOerPJ0NF5IArDBvQ_dX3ODWnln5c&language={{app()->getLocale()}}&libraries=places&sensor=true&callback=initmandoubMap"
        async defer></script>
    <script>
        $(document).ready(function () {
            $("iframe[name='htmlComp-iframe']").attr("allow", "geolocation");
            $("body")
                .parent()
                .prev()
                .attr("allow", "geolocation");
        });
        var map
        var marker
        var input = document.getElementById('mandoubaddress');
        var infowindow
        var geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);

        function geolocate() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function (position) {

                    var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    marker.setPosition(pos)
                    map.setCenter(pos);
                    geocoder.geocode({'latLng': pos}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {

                                document.getElementById('mandoubaddress').value = results[0].formatted_address;
                                document.getElementById('mandoub_latitude').value = position.coords.latitude;
                                document.getElementById('mandoub_longitude').value = position.coords.longitude;
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
            }
        }

        function GeolocationControl(controlDiv, map) {

            // Set CSS for the control button
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#444';
            controlUI.style.borderStyle = 'solid';
            controlUI.style.borderWidth = '1px';
            controlUI.style.borderColor = 'white';
            controlUI.style.height = '28px';
            controlUI.style.marginBottom = '15px';
            controlUI.style.cursor = 'pointer';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to center map on your location';
            controlUI.id = 'goCenterUI';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control text
            var controlText = document.createElement('div');
            controlText.style.fontFamily = 'Arial,sans-serif';
            controlText.style.fontSize = '10px';
            controlText.style.color = 'white';
            controlText.style.paddingLeft = '10px';
            controlText.style.paddingRight = '10px';
            controlText.style.marginTop = '8px';
            controlText.innerHTML = 'Center map on your location';
            controlUI.appendChild(controlText);

            // Setup the click event listeners to geolocate user
            google.maps.event.addDomListener(controlUI, 'click', geolocate);
        }

        google.maps.event.addDomListener(window, "load", initmandoubMap);

        function initmandoubMap() {


            infowindow = new google.maps.InfoWindow();
            var m = document.getElementById('mandoub_map');
            var lng = m.getAttribute('lng');
            var lat = m.getAttribute('lat');
            var latlng = new google.maps.LatLng(lat, lng);
            map = new google.maps.Map(document.getElementById('mandoub_map'), {
                center: latlng,
                zoom: 13,
                mapTypeControl: false,
                fullscreenControl: false,
                streetViewControl: false
            });

            marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });

            var geolocationDiv = document.createElement('div');
            var geolocationControl = new GeolocationControl(geolocationDiv, map);
            geolocationDiv.index = 1;
            geolocationDiv.style['padding-top'] = '10px';
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(geolocationDiv);
            geocoder = new google.maps.Geocoder();
            autocomplete = new google.maps.places.Autocomplete(input);

            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);


            if (navigator.geolocation) {
                console.log("geolocation ok");
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    // infoWindow.setPosition(pos);
                    // infoWindow.setContent('Location found.');
                    // infoWindow.open(map);
                    map.setCenter(pos);
                    console.log(position.coords.latitude, position.coords.longitude);
                    geocoder.geocode({'latLng': pos}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                bindmandoubDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                    // map.removeMarker(map.markers[0]);
                    marker.setPosition(pos)
                    // marker = new google.maps.Marker({
                    //     position: pos,
                    //     map: map,
                    //     draggable:true,
                    //     animation: google.maps.Animation.DROP,
                    // });
                    // marker.setMap(map);
                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }

            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                bindmandoubDataToForm(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng());
                infowindow.setContent(place.formatted_address);
                infowindow.open(map, marker);

            });
            // this function will work on marker move event into map
            google.maps.event.addListener(marker, 'dragend', function () {
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            bindmandoubDataToForm(results[0].formatted_address, marker.getPosition().lat(), marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }

        function bindmandoubDataToForm(address, lat, lng) {
            document.getElementById('mandoubaddress').value = address;
            document.getElementById('mandoub_latitude').value = lat;
            document.getElementById('mandoub_longitude').value = lng;
        }


    </script>

@endsection
