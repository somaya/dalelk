@extends('layouts.master')
@section('content')

                <form class="form" action="{{route('sendCode')}}" method="post">
                {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="form-group">
                                <label class="label-control">رقم الهاتف  </label>
                                <input name="phone" type="number" class="form-control" placeholder="05xxxxxxxx" />
                                @if(session('errormsg'))
                                    <div class="text-danger" role="alert">
                                    {{ session('errormsg') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12">
                            <button type="submit" class="btn btn-send">أرسل كود التحقق</button>
                        </div>
                    </div>
                </form>

@endsection