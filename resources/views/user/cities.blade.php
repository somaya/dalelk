<option selected>إختر المدينة * Select City</option>
@foreach($cities as $city)
    <option {{old('city_id')&&old('city_id')==$city->id?'selected':''}} value="{{$city->id}}">{{$city->name_ar .' * '. $city->name_en}}</option>
@endforeach