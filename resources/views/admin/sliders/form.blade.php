<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">العنوان باللغه العربيه*: </label>
    <div class="col-lg-5{{ $errors->has('title_ar') ? ' has-danger' : '' }}">
        {!! Form::text('title_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"العنوان باللغه العربيه"]) !!}
        @if ($errors->has('title_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">العنوان باللغه الانجليزيه*: </label>
    <div class="col-lg-5{{ $errors->has('title_en') ? ' has-danger' : '' }}">
        {!! Form::text('title_en',null,['class'=>'form-control m-input','autofocus','placeholder'=>"العنوان باللغه الانجليزيه"]) !!}
        @if ($errors->has('title_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('title_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصوره: </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($slider) && $slider->photo)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$slider->photo}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif




