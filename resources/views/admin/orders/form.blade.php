

<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">طريقه الدفع: </label>
    <div class="col-lg-5{{ $errors->has('payment_type') ? ' has-danger' : '' }}">
        {!! Form::text('payment_type',null,['class'=>'form-control m-input','disabled']) !!}

    </div>
    <label class="col-lg-1 col-form-label">رقم الطلب: </label>
    <div class="col-lg-5{{ $errors->has('discount') ? ' has-danger' : '' }}">
        {!! Form::text('order_number',null,['class'=>'form-control m-input','disabled']) !!}

    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">وقت التوصيل: </label>
    <div class="col-lg-5{{ $errors->has('delivary_time') ? ' has-danger' : '' }}">
        {!! Form::text('delivary_time',null,['class'=>'form-control m-input','disabled']) !!}

    </div>
    <label class="col-lg-1 col-form-label">الحاله: </label>
    <div class="col-lg-5{{ $errors->has('status') ? ' has-danger' : '' }}">
        {!! Form::text('status',null,['class'=>'form-control m-input','disabled']) !!}

    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">تفاصيل الطلب: </label>
    <div class="col-lg-5{{ $errors->has('details') ? ' has-danger' : '' }}">
        {!! Form::text('details',null,['class'=>'form-control m-input','disabled']) !!}

    </div>


</div>
<div class="row">
    <label class="col-lg-2 col-form-label">الصور: </label>


    @if(isset($order) && $order->images)
        @foreach ($order->images as $index=> $image)


            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">
                {{--<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">--}}

                {{--<span class="fa fa-trash project-property-image-delete"--}}
                      {{--id="{{$image->id}}"--}}
                      {{--style="position: absolute;top: 6px;right: 30px;z-index: 100;font-size:25px;color:black"--}}
                      {{--onmousemove="this.style.color='red'"--}}
                      {{--onmouseout="this.style.color='black'"></span>--}}
                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{$image->image}}"
                        style="height: 70%; width: 70%"
                        data-holder-rendered="true">
            </div>
            <br>
        @endforeach
    @endif
</div>





