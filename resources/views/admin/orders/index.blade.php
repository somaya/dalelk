@extends('admin.layouts.app')
@section('title')
    الطلبات
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">الطلبات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الطلبات
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {{--<div><a href="{{route('orders.create')}}" style="margin-bottom:20px"--}}
                    {{--class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i> اضف كوبون</a></div>--}}


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th>#</th>
                    <th>رقم الطلب</th>
                    <th>المتجر</th>
                    <th>صاحب الطلب</th>
                    <th>الحاله</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $index=>$order)
                    <tr>
                        <td>{{$index + $orders->firstItem()}}</td>
                        <td>{{$order->order_number}}</td>
                        <td>{{$order->store?$order->store->name_ar:$order->place_id}}</td>
                        <td>{{$order->user->name}}</td>
                        <td>{{$order->status}}</td>
                        <td>
                            <a title="عرض التفاصيل" href="/webadmin/orders/{{$order->id}}"><i
                                    class="fa fa-eye"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/orders/{{ $order->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$orders->links()}}
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/countrcountriess!!}--}}
@endsection
