@extends("admin.layouts.app")
@section('title')
    لوحة تحكم تدلل
@endsection
@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="page-title">اهلا!</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">لوحة التحكم</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$users}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/users"><h6 class="text-muted">الاعضاء</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$users}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$orders}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/orders"><h6 class="text-muted">الطلبات</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$orders}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$offers}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/offers"><h6 class="text-muted">العروض</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$offers}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$deliveries}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/offers"><h6 class="text-muted">المناديب</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$deliveries}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
