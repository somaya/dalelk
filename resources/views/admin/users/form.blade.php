
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label"> الاسم</label>
    <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','placeholder'=> 'الاسم' ]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">الهاتف</label>
    <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'الهاتف' ]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>




</div>


<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">البريد الالكتروني</label>
    <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','placeholder'=> 'البريد الالكترونى' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

</div>




<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">Profile Photo</label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($user) && $user->photo)
<div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{asset($user->photo)}}"
                        style="height: 150px; width: 150px"
                        data-holder-rendered="true">
            </div>

</div>
@endif


