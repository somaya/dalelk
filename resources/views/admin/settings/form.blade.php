<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">لوجو التطبيق</label>
    <div class="col-lg-11{{ $errors->has('logo') ? ' has-danger' : '' }}">

        <input type="file" name="logo" class="form-control uploadinput">
        @if ($errors->has('logo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
    </div>
</div>
@if(isset($settings) && $settings->logo)
    <input type="hidden" value="{{ $settings->logo }}" name="logo">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px; text-align: center">
            <img
                data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                alt="First slide [800x4a00]"
                src="{{asset($settings->logo)}}"
                style="height: 150px; width: 150px"
                data-holder-rendered="true">
        </div>
    </div>
@endif


<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">نسبة ربح التطبيق</label>
    <div class="col-lg-11{{ $errors->has('commission') ? ' has-danger' : '' }}">
        <input type="number" name="commission" class="form-control" value="{{$settings->commission}}">
        @if ($errors->has('commission'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('commission') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">مساحة تغطية وصول الطلبات للمناديب بالكيلو</label>
    <div class="col-lg-11{{ $errors->has('delivery_distance') ? ' has-danger' : '' }}">
        <input type="number" name="delivery_distance" class="form-control" value="{{$settings->delivery_distance}}">
        @if ($errors->has('delivery_distance'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('delivery_distance') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> شروط الاستخدام باللغه العربيه: </label>
    <div class="col-lg-10{{ $errors->has('terms_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('terms_ar',old('terms_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'شروط الاستخدام باللغه العربيه' ]) !!}
        @if ($errors->has('terms_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('terms_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> شروط الاستخدام باللغه الانجليزيه: </label>
    <div class="col-lg-10{{ $errors->has('terms_en') ? ' has-danger' : '' }}">
        {!! Form::textarea('terms_en',old('terms_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'شروط الاستخدام باللغه الانجليزيه' ]) !!}
        @if ($errors->has('terms_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('terms_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> سياسة الخصوصيه باللغه العربيه: </label>
    <div class="col-lg-10{{ $errors->has('privacy_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('privacy_ar',old('privacy_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'سياسة الخصوصيه باللغه العربيه' ]) !!}
        @if ($errors->has('privacy_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('privacy_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> سياسة الخصوصيه باللغه الانجليزيه: </label>
    <div class="col-lg-10{{ $errors->has('privacy_en') ? ' has-danger' : '' }}">
        {!! Form::textarea('privacy_en',old('privacy_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'سياسة الخصوصيه باللغه الانجليزيه' ]) !!}
        @if ($errors->has('privacy_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('privacy_en') }}</strong>
            </span>
        @endif
    </div>
</div>


