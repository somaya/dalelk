<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">اسم المتجر باللغه العربيه*: </label>
    <div class="col-lg-5{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>" اسم المتجر باللغه العربيه"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">اسم المتجر باللغه الانجليزيه*: </label>
    <div class="col-lg-5{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم المتجر باللغه الانجليزيه"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">القسم* : </label>
    <div class="col-lg-10{{ $errors->has('category_id') ? ' has-danger' : '' }}">
        <select name="category_id" id="category_id" data-placeholder="City" class="form-control ">
            <option value="">اختر القسم</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}" {{isset($store) && $category->id==$store->category_id ?'selected':''}}>{{$category->name_ar}} </option>
            @endforeach


        </select>
        @if ($errors->has('category_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label">ساعات العمل *: </label>
    <div class="col-lg-10{{ $errors->has('work_hours') ? ' has-danger' : '' }}">
        <textarea name="work_hours" class="form-control summernote " >{{isset($store) ? $store->work_hours :old('work_hours')}}</textarea>

        @if ($errors->has('work_hours'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('work_hours') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اللوجو: </label>
    <div class="col-lg-10{{ $errors->has('logo') ? ' has-danger' : '' }}">

        <input type="file" name="logo"   class="form-control uploadinput">
        @if ($errors->has('logo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($store) && $store->logo)
    <div class="row">




        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$store->logo}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">العنوان* : </label>
    <div class="col-lg-10{{ $errors->has('address') ? ' has-danger' : '' }}">
        <input type="text" value="{{isset($store)? $store->address:''}}" name="address" id="storeaddress" class="form-control">

        <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
        <div id="store_map" lat="{{isset($store)? $store->latitude:'30.044420'}}" lng="{{isset($store) ? $store->langitude:'31.235712'}}" style="width: 100%; height: 400px;"></div>
        <input type="hidden"  id="store_latitude" name="latitude" value="{{isset($store)? $store->latitude:'30.044420'}}"/>
        <input type="hidden"  id="store_longitude" name="langitude"  value="{{isset($store) ? $store->langitude:'31.235712'}}"/>
        @if ($errors->has('address'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>

</div>





