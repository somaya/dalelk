@extends('admin.layouts.app')

@section('title')
    تعديل متجر
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/stores')}}" class="m-menu__link">
            <span class="m-menu__link-text">المتاجر </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">تعديل متجر </span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تعديل متجر
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($store,['route' => ['stores.update' , $store->id],'method'=> 'PATCH','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">
            @include('admin.stores.form')
        </div>
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success">تعديل</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaFbSOerPJ0NF5IArDBvQ_dX3ODWnln5c&language={{app()->getLocale()}}&libraries=places&sensor=true&callback=initstoreMap"
            async defer></script>
    <script>
        $(document).ready(function() {
            $("iframe[name='htmlComp-iframe']").attr("allow", "geolocation");
            $("body")
                .parent()
                .prev()
                .attr("allow", "geolocation");
        });
        var map
        var marker
        var input = document.getElementById('storeaddress');
        var infowindow
        var geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);

        //        function geolocate() {
        //
        //            if (navigator.geolocation) {
        //
        //                navigator.geolocation.getCurrentPosition(function (position) {
        //
        //                    var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        //                    marker.setPosition(pos)
        //                    map.setCenter(pos);
        //                    geocoder.geocode({'latLng': pos}, function(results, status) {
        //                        if (status == google.maps.GeocoderStatus.OK) {
        //                            if (results[0]) {
        //
        //                                document.getElementById('storeaddress').value = results[0].formatted_address;
        //                                document.getElementById('store_latitude').value = position.coords.latitude;
        //                                document.getElementById('store_longitude').value = position.coords.longitude;
        //                                infowindow.setContent(results[0].formatted_address);
        //                                infowindow.open(map, marker);
        //                            }
        //                        }
        //                    });
        //                });
        //            }
        //        }
        //
        //        function GeolocationControl(controlDiv, map) {
        //
        //            // Set CSS for the control button
        //            var controlUI = document.createElement('div');
        //            controlUI.style.backgroundColor = '#444';
        //            controlUI.style.borderStyle = 'solid';
        //            controlUI.style.borderWidth = '1px';
        //            controlUI.style.borderColor = 'white';
        //            controlUI.style.height = '28px';
        //            controlUI.style.marginBottom = '15px';
        //            controlUI.style.cursor = 'pointer';
        //            controlUI.style.textAlign = 'center';
        //            controlUI.title = 'Click to center map on your location';
        //            controlUI.id = 'goCenterUI';
        //            controlDiv.appendChild(controlUI);
        //
        //            // Set CSS for the control text
        //            var controlText = document.createElement('div');
        //            controlText.style.fontFamily = 'Arial,sans-serif';
        //            controlText.style.fontSize = '10px';
        //            controlText.style.color = 'white';
        //            controlText.style.paddingLeft = '10px';
        //            controlText.style.paddingRight = '10px';
        //            controlText.style.marginTop = '8px';
        //            controlText.innerHTML = 'Center map on your location';
        //            controlUI.appendChild(controlText);
        //
        //            // Setup the click event listeners to geolocate user
        //            google.maps.event.addDomListener(controlUI, 'click', geolocate);
        //        }
        google.maps.event.addDomListener(window, "load", initstoreMap);
        function initstoreMap()
        {


            infowindow = new google.maps.InfoWindow();
            var m=document.getElementById('store_map');
            var lng= m.getAttribute('lng');
            var lat= m.getAttribute('lat');
            var latlng = new google.maps.LatLng(lat,lng);
            map = new google.maps.Map(document.getElementById('store_map'), {
                center: latlng,
                zoom: 13,
                mapTypeControl: false,
                fullscreenControl: false,
                streetViewControl: false
            });

            marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });

            var geolocationDiv = document.createElement('div');
//            var geolocationControl = new GeolocationControl(geolocationDiv, map);
            geolocationDiv.index = 1;
            geolocationDiv.style['padding-top'] = '10px';
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(geolocationDiv);
            geocoder = new google.maps.Geocoder();
            autocomplete = new google.maps.places.Autocomplete(input);

            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);



//            if (navigator.geolocation) {
//                console.log("geolocation ok");
//                navigator.geolocation.getCurrentPosition(function(position)
//                {
//                    var pos = {
//                        lat: position.coords.latitude,
//                        lng: position.coords.longitude
//                    };
//                    // infoWindow.setPosition(pos);
//                    // infoWindow.setContent('Location found.');
//                    // infoWindow.open(map);
//                    map.setCenter(pos);
//                    console.log(position.coords.latitude ,position.coords.longitude);
//                    geocoder.geocode({'latLng': pos}, function(results, status) {
//                        if (status == google.maps.GeocoderStatus.OK) {
//                            if (results[0]) {
//                                bindstoreDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
//                                infowindow.setContent(results[0].formatted_address);
//                                infowindow.open(map, marker);
//                            }
//                        }
//                    });
//                    // map.removeMarker(map.markers[0]);
//                    marker.setPosition(pos)
//                    // marker = new google.maps.Marker({
//                    //     position: pos,
//                    //     map: map,
//                    //     draggable:true,
//                    //     animation: google.maps.Animation.DROP,
//                    // });
//                    // marker.setMap(map);
//                }, function() {
//                    handleLocationError(true, infoWindow, map.getCenter());
//                });
//            }else {
//                // Browser doesn't support Geolocation
//                handleLocationError(false, infoWindow, map.getCenter());
//            }
//            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
//                infoWindow.setPosition(pos);
//                infoWindow.setContent(browserHasGeolocation ?
//                    'Error: The Geolocation service failed.' :
//                    'Error: Your browser doesn\'t support geolocation.');
//                infoWindow.open(map);
//            }
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                bindstoreDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
                infowindow.setContent(place.formatted_address);
                infowindow.open(map, marker);

            });
            // this function will work on marker move event into map
            google.maps.event.addListener(marker, 'dragend', function() {
                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            bindstoreDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }
        function bindstoreDataToForm(address,lat,lng){
            document.getElementById('storeaddress').value = address;
            document.getElementById('store_latitude').value = lat;
            document.getElementById('store_longitude').value = lng;
        }



    </script>
@endsection

