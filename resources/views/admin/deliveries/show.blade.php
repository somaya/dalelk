@extends('admin.layouts.app')

@section('title')
    تفاصيل المندوب
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/users')}}" class="m-menu__link">
            <span class="m-menu__link-text">المناديب</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل المندوب</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل المندوب
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($user,['route' => ['users.show' , $user->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الاسم </label>
                <div class="col-lg-5{{ $errors->has('name') ? ' has-danger' : '' }}">
                    {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}
                </div>
                <label class="col-lg-1 col-form-label">الهاتف</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">البريد الالكترونى</label>
                <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
                    {!! Form::text('email',old('email'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>

                <label class="col-lg-1 col-form-label">رقم الهوية</label>
                <div class="col-lg-5{{ $errors->has('national_id') ? ' has-danger' : '' }}">
                    {!! Form::text('national_id',old('national_id'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">العنوان</label>
                <div class="col-lg-11{{ $errors->has('address') ? ' has-danger' : '' }}">
                    {!! Form::text('address',old('address'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">اسم البنك</label>
                <div class="col-lg-5{{ $errors->has('bank') ? ' has-danger' : '' }}">
                    {!! Form::text('bank',old('bank'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>

                <label class="col-lg-1 col-form-label">رقم الحساب البنكي</label>
                <div class="col-lg-5{{ $errors->has('bank_number') ? ' has-danger' : '' }}">
                    {!! Form::text('bank_number',old('bank_number'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الصورة الشخصية</label>
                @if(isset($user) && $user->photo)
                    <div class="col-lg-5 col-md-5 col-sm-5" style="margin-bottom: 10px;">
                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->photo)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                @endif


                <label class="col-lg-1 col-form-label">صورة الهوية</label>
                @if(isset($user) && $user->identity_card_image)
                    <div class="col-lg-5 col-md-5 col-sm-5" style="margin-bottom: 10px;">
                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->identity_card_image)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                @endif
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">صورة مقدمة السيارة</label>
                @if(isset($user) && $user->car_front)
                    <div class="col-lg-5 col-md-5 col-sm-5" style="margin-bottom: 10px;">
                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->car_front)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                @endif


                <label class="col-lg-1 col-form-label">صورة خلفية للسيارة</label>
                @if(isset($user) && $user->car_back)
                    <div class="col-lg-5 col-md-5 col-sm-5" style="margin-bottom: 10px;">
                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->car_back)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                @endif
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">صورة رخصة القيادة</label>
                @if(isset($user) && $user->driving_license)
                    <div class="col-lg-5 col-md-5 col-sm-5" style="margin-bottom: 10px;">
                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->driving_license)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                @endif
            </div>

        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

