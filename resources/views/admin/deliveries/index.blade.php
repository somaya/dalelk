@extends('admin.layouts.app')
@section('title')
    المناديب
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/deliveries')}}" class="m-menu__link">
            <span class="m-menu__link-text">المناديب</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        المناديب
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
        {{--            <div><a href="{{route('deliveries.create')}}" style="margin-bottom:20px"--}}
        {{--                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i>اضافة عضو</a></div>--}}
        {{--            <br>--}}


        <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>رقم الهوية</th>
                    <th>العنوان</th>
                    <th>الادوات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $index=> $user)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$user->name}} </td>
                        <td>{{$user->phone}} </td>
                        <td>{{$user->national_id ?:"--------"}} </td>
                        <td>{{$user->address ?:"--------"}} </td>
                        <td>

                            <a title="عرض" href="/webadmin/deliveries/{{$user->id}}"><i class="fa fa-eye"></i></a>

                            <form class="inline-form-style"
                                  action="/webadmin/deliveries/{{ $user->id }}"
                                  method="post">
                                {!!  method_field('PATCH') !!}
                                @if($user->active == 1)
                                    <button type="submit" class="trash-btn">
                                        <span title="ايقاف" class="fa fa-stop"></span>
                                    </button>
                                @else
                                    <button type="submit" class="trash-btn">
                                        <span title="تفعيل" class="fa fa-check"></span>
                                    </button>
                                @endif
                                <input type="hidden" name="active" value="{{($user->active == 1) ? 0 : 1 }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>


                            <form class="inline-form-style"
                                  action="/webadmin/deliveries/{{ $user->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span title="حذف" class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
