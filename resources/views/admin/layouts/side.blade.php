<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الرئيسية</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/settings/1/edit')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادات</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/users')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعضاء</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/admins')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-users-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدراء</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/deliveries')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user-ok"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المناديب</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/categories')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/sliders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-shapes"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاسليدرات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/stores')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-buildings"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المتاجر</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/coupons')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-coins"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الكوبونات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/orders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-plus"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الطلبات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/offers')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user-ok"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">العروض</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/invoices')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-information"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الفواتير</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/contacts" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-email"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">اتصل بنا</span>
            </span>
        </span>
    </a>
</li>

