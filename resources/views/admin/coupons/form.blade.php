{{--<div class="form-group m-form__group row">--}}
    {{--<label class="col-lg-2 col-form-label">المتجر* : </label>--}}
    {{--<div class="col-lg-10{{ $errors->has('store_id') ? ' has-danger' : '' }}">--}}
        {{--<select name="store_id" id="store_id" data-placeholder="User" class="form-control select-minimum ">--}}
            {{--<option value="">اختر المتجر</option>--}}
            {{--<optgroup label="المتجر">--}}
                {{--@foreach($stores as $store)--}}
                    {{--<option value="{{$store->id}}"{{old('store_id')==$store->id ? 'selected':''}} {{isset($coupon) && $store->id==$coupon->store_id ?'selected':''}}>{{$store->name_ar}}</option>--}}
                {{--@endforeach--}}
            {{--</optgroup>--}}


        {{--</select>--}}
        {{--@if ($errors->has('store_id'))--}}
            {{--<span class="form-control-feedback" role="alert">--}}
                {{--<strong>{{ $errors->first('store_id') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}

{{--</div>--}}

<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">الكود*: </label>
    <div class="col-lg-5{{ $errors->has('code') ? ' has-danger' : '' }}">
        {!! Form::text('code',null,['class'=>'form-control m-input','placeholder'=>"الكود"]) !!}
        @if ($errors->has('code'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('code') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">الخصم *: </label>
    <div class="col-lg-5{{ $errors->has('discount') ? ' has-danger' : '' }}">
        {!! Form::number('discount',null,['class'=>'form-control m-input','placeholder'=>"الخصم"]) !!}
        @if ($errors->has('discount'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('discount') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">تاريخ انتهاء الصلاحيه*: </label>
    <div class="col-lg-10{{ $errors->has('expired_at') ? ' has-danger' : '' }}">
        {!! Form::date('expired_at',null,['class'=>'form-control m-input']) !!}
        @if ($errors->has('expired_at'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('expired_at') }}</strong>
            </span>
        @endif
    </div>


</div>





