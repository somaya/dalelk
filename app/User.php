<?php

namespace App;

use App\Models\Captain_rate;
use App\Models\User_rates;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function captain_rates()
    {
        return $this->hasMany(Captain_rate::class, 'captain_id');
    }
    public function user_rates()
    {
        return $this->hasMany(User_rates::class, 'user_id');
    }
}
