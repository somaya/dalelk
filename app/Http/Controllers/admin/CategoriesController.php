<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::orderBy('created_at','ASC')->paginate(10);
        return view('admin.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'photo' => 'required',
        ]);
       $category= Category::create($request->except('photo'));

        if ($request->hasFile('photo'))
        {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/images/categories/', $imageName
            );
            $category->photo = '/images/categories/' . $imageName;
            $category->save();

        }
        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة القسم بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::find($id);
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        $category=Category::find($id);
        $category->update($request->except('photo'));

        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/images/categories/', $imageName
            );
            $category->photo = '/images/categories/' . $imageName;
            $category->save();

        }
        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل القسم بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف القسم بنجاح']));

    }
}
