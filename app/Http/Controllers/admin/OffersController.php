<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    public function index()
    {
        $offers=Offer::orderBy('created_at','ASC')->paginate(10);
        return view('admin.offers.index',compact('offers'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offer=Offer::find($id);
        return view('admin.offers.show',compact('offer'));

    }


    public function destroy($id)
    {
        Offer::destroy($id);
        return redirect('/webadmin/offers')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف العرض بنجاح']));

    }
}
