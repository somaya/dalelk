<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Store;
use Illuminate\Http\Request;

class CouponsController extends Controller
{
    public function index()
    {
        $coupons=Coupon::orderBy('created_at','ASC')->paginate(10);
        return view('admin.coupons.index',compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores=Store::all();
        return view('admin.coupons.add',compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

//            'store_id' => 'required',
            'code' => 'required',
            'discount' => 'required',
            'expired_at' => 'required',
        ]);
        Coupon::create($request->all());

        return redirect('/webadmin/coupons')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الكوبون بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon=Coupon::find($id);
//        $stores=Store::all();
        return view('admin.coupons.edit',compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

//            'store_id' => 'required',
            'code' => 'required',
            'discount' => 'required',
            'expired_at' => 'required',
        ]);
        $coupon=Coupon::find($id);
        $coupon->update($request->all());

        return redirect('/webadmin/coupons')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الكوبون بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::destroy($id);
        return redirect('/webadmin/coupons')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الكوبون بنجاح']));

    }
}
