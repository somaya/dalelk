<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StoresController extends Controller
{
    public function index()
    {
        $stores=Store::orderBy('created_at','ASC')->paginate(10);
        return view('admin.stores.index',compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('admin.stores.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'logo' => 'required',
            'category_id' => 'required',
            'work_hours' => 'required',
            'address' => 'required',

        ]);
        $store= Store::create($request->except('logo'));

        if ($request->hasFile('logo'))
        {
            $request->validate([

                'logo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('logo')->extension();
            $request->file('logo')->move(
                base_path() . '/public/images/stores/', $imageName
            );
            $store->logo = '/images/stores/' . $imageName;
            $store->save();

        }
        return redirect('/webadmin/stores')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة المتجر بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store=Store::find($id);
        $categories=Category::all();
        return view('admin.stores.edit',compact('store','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
//            'logo' => 'required',
            'category_id' => 'required',
            'work_hours' => 'required',
            'address' => 'required',

        ]);
        $store=Store::find($id);
        $store->update($request->except('logo'));

        if ($request->hasFile('logo')) {
            $request->validate([

                'logo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('logo')->extension();
            $request->file('logo')->move(
                base_path() . '/public/images/stores/', $imageName
            );
            $store->logo = '/images/stores/' . $imageName;
            $store->save();

        }
        return redirect('/webadmin/stores')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل المتجر بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Store::destroy($id);
        return redirect('/webadmin/stores')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف المتجر بنجاح']));

    }
}
