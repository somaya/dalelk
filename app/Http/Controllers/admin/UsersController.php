<?php

namespace App\Http\Controllers\admin;

use App\Models\BankData;
use App\Models\CarOrder;
use App\Models\City;
use App\Models\Country;
use App\Models\MobileData;
use App\Models\PropertyOrder;
use App\Models\Transfer;
use App\Notifications\BookingRequestReceived;
use App\Notifications\FromAdmin;
use App\User;
use Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Notification;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('role','<>',1)
            ->where('type', 1)
            ->get();
        return view('admin.users.index', compact('users'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
//            'password' => 'required',
//            'address' => 'required',
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
//            'password' => Hash::make($request->password),
//            'address' => $request->address,
            'role' => 2,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . 'public/images/users/', $imageName
            );
            $user->photo = '/images/users/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة العضو بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required',
            'phone' => 'required|numeric',
//            'address' => 'required',
        ]);
        $user = User::where('id', $id)->first();
        $user->update([
            'phone' => $request->phone,
            'name' => $request->name,
//            'address' => $request->address,
        ]);
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $user->update([
                'email' => $request->email
            ]);

        }
//        if ($request->password != '') {
//            $user->update([
//                'password' => Hash::make($request->password),
//            ]);
//        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/images/users/', $imageName
            );
            $user->photo = '/images/users/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/users' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل العضو بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->email = $user->email . '_deleted';
        $user->save();
        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف العضو بنجاح']));
    }


}
