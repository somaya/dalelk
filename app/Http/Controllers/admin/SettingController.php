<?php

namespace App\Http\Controllers\admin;

use App\Models\Setting;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function edit($id)
    {
        $settings = Setting::find($id);
        return view('admin.settings.edit', compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'privacy_en' => 'required',
            'privacy_ar' => 'required',
            'terms_ar' => 'required',
            'terms_en' => 'required',
            'commission' => 'required',
            'delivery_distance' => 'required',
        ]);

        $settings = Setting::find($id);
        $inputs = $request->all();

        if ($request->hasFile('logo')) {
            $request->validate([
                'logo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);


            $imageName = Str::random(10) . '.' . $request->file('logo')->extension();
            $request->file('logo')->move(
                base_path() . '/public/uploads/', $imageName
            );
            if ($settings->logo) {
                if (File::exists($settings->logo)) {
                    unlink($settings->logo);
                }
            }
            $inputs['logo'] = 'uploads/' . $imageName;
        }

        $settings->update($inputs);

        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الاعدادات بنجاح']));
    }

}
