<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BankData;
use App\Models\CarOrder;
use App\Models\City;
use App\Models\Country;
use App\Models\MobileData;
use App\Models\PropertyOrder;
use App\Models\Transfer;
use App\Notifications\BookingRequestReceived;
use App\Notifications\FromAdmin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Notification;

class DeliveriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('role', '<>', 1)
            ->where('type', 2)
            ->get();
        return view('admin.deliveries.index', compact('users'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.deliveries.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
//            'password' => 'required',
//            'address' => 'required',
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
//            'password' => Hash::make($request->password),
//            'address' => $request->address,
            'role' => 2,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . 'public/images/users/', $imageName
            );
            $user->photo = '/images/users/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/deliveries')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة العضو بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.deliveries.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'active' => $request->active
        ]);
//        $request->validate([
//
//            'name' => 'required',
//            'phone' => 'required|numeric',
////            'address' => 'required',
//        ]);
//        $user = User::where('id', $id)->first();
//        $user->update([
//            'phone' => $request->phone,
//            'name' => $request->name,
////            'address' => $request->address,
//        ]);
//        if ($request->email != $user->email) {
//            $this->validate($request, [
//                'email' => 'email|unique:users',
//            ]);
//            $user->update([
//                'email' => $request->email
//            ]);
//
//        }
////        if ($request->password != '') {
////            $user->update([
////                'password' => Hash::make($request->password),
////            ]);
////        }
//        if ($request->hasFile('photo')) {
//            $request->validate([
//
//                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
//
//            ]);
//
//            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
//            $request->file('photo')->move(
//                base_path() . '/public/images/users/', $imageName
//            );
//            $user->photo = '/images/users/' . $imageName;
//            $user->save();
//        }
        return redirect('/webadmin/deliveries')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم التعديل بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف المندوب بنجاح']));
    }


}
