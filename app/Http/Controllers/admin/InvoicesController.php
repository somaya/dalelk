<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;

class InvoicesController extends Controller
{
    public function index()
    {
        $invoices=Invoice::orderBy('created_at','ASC')->paginate(10);
        return view('admin.invoices.index',compact('invoices'));
    }



    public function destroy($id)
    {
        Invoice::destroy($id);
        return redirect('/webadmin/invoices')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الفاتوره بنجاح']));

    }
}
