<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Models\Choice;
use App\Models\Company;
use App\Models\ContractRequest;
use App\Models\Maintenance;
use App\Models\Meeting;
use App\Models\Offer;
use App\Models\Order;
use App\Models\PriceRequest;
use App\Models\Product;
use App\Models\RequestService;
use App\Models\Startup;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {

        $users = User::where('role', '<>', 1)->where('type', 1)->count();
        $orders = Order::count();
        $offers = Offer::count();
        $deliveries = User::where('role', '<>', 1)->where('type', 2)->count();

        return view('admin.index', compact('users', 'orders', 'offers', 'deliveries'));
    }
}
