<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
        $orders=Order::orderBy('created_at','ASC')->paginate(10);
        return view('admin.orders.index',compact('orders'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order=Order::find($id);
        return view('admin.orders.show',compact('order'));

    }


    public function destroy($id)
    {
        Order::destroy($id);
        return redirect('/webadmin/orders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الطلب بنجاح']));

    }
}
