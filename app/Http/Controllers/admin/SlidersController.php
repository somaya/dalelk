<?php

namespace App\Http\Controllers\admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SlidersController extends Controller
{
    public function index()
    {
        $sliders=Slider::orderBy('created_at','ASC')->get();
        return view('admin.sliders.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'title_ar' => 'required',
            'title_en' => 'required',
            'photo' => 'required',
        ]);
        $slider= Slider::create($request->except('photo'));

        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/sliders/', $imageName
            );
            $slider->photo = '/uploads/sliders/' . $imageName;
            $slider->save();

        }
        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الاسلايدر بنجاح']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider=Slider::find($id);
        return view('admin.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'title_ar' => 'required',
            'title_en' => 'required',
        ]);
        $slider=Slider::find($id);
        $slider->update($request->except('photo'));

        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/sliders/', $imageName
            );
            $slider->photo = '/uploads/sliders/' . $imageName;
            $slider->save();

        }
        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الاسلايدر بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::destroy($id);
        return redirect('/webadmin/sliders')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف الاسلايدر بنجاح']));

    }
}
