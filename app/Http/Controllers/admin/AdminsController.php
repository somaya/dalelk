<?php

namespace App\Http\Controllers\admin;


use App\Models\Country;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminsController extends Controller
{
    public function index()
    {
        $admins = User::where('role',1)->get();
        return view('admin.admins.index', compact('admins'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
//            'password' => 'required',
//            'address' => 'required',
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
//            'password' => Hash::make($request->password),
//            'address' => $request->address,
            'role' => 1,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/admins')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة المدير بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.admins.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.admins.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required',
            'phone' => 'required|numeric',
//            'address' => 'required',
        ]);
        $user = User::where('id', $id)->first();
        $user->update([
            'phone' => $request->phone,
            'name' => $request->name,
            'address' => $request->address,
        ]);
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|unique:admins',
            ]);
            $user->update([
                'email' => $request->email
            ]);

        }
//        if ($request->password != '') {
//            $user->update([
//                'password' => Hash::make($request->password),
//            ]);
//        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName =Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/admins' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل المدير بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->email = $user->email . '_deleted';
        $user->save();
        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف المدير بنجاح']));
    }

}
