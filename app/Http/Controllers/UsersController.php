<?php

namespace App\Http\Controllers;

use App\citcCarTypes;
use App\City;
use App\Comments;
use App\Country;
use App\Coupons;
use App\Exports\PaymentsExport;
use App\Exports\UsersExport;
use App\Mail\PublicMessage;
use App\Models\Setting;
use App\Nationality;
use App\Payments;
use App\Region;
use App\Role;
use App\SmsEmailNotification;
use App\User;
use App\userBlocks;
use App\userDevices;
use App\userMeta;
use App\usersCoupons;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use Session;
use Validator;

class UsersController extends Controller
{

    public function mandoub($user_id = 0)
    {
        $user = User::find($user_id);
        $settings = Setting::find(1);
        $site_title = 'Dalilk';
        $site_percentage = 10;
//        $cities = City::all();
//        $regions = Region::all();
//        $nationalities = Nationality::all();
//        $car_types = citcCarTypes::all();
        return view('user.signup', compact('user', 'site_title', 'site_percentage', 'settings'));
    }

    public function delegation()
    {
        Auth::logout();
        return view('user.delegation');
    }

    public function sendCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:255',
        ]);
        if ($validator->passes()) {

            $msg = trans('auth.yourcode') . '' . setting('site_title') . ' : ';
            $number = convert2english(request('phone'));
            $phone = phoneValidate($number);
            // if (substr($number, 0, 1) === '0'){
            //     $number = substr($number, 1);
            // }
            // $phone         = preg_replace('/\s+/', '', $number);

            if ($user = User::where(['phone' => $phone])->first()) {
                $userMeta = userMeta::where(['phone' => $phone])->first();
                if ($user->delegate == 'true') {
                    return back()->with('errormsg', 'هذا الحساب مسجل كمندوب بالفعل.');
                }
                if (isset($userMeta)) {
                    if ($userMeta->status == null) {
                        return back()->with('errormsg', 'تم تقديم طلب بالفعل يرجى انتظار الموافقة علية من الادراة');
                    }
                }

                if (setting('v_code_generate') == 'false' && setting('v_code') != NULL) {
                    $user->code = setting('v_code');
                } else {
                    $user->code = generate_code();
                }
                $user->save();
                $msg = $msg . $user->code;
                $phone = $user->phone;
                $key = $user->phonekey;
                send_mobile_sms($key . $phone, $msg);
                $phone = $user->phone;
                Session::put('phone', $phone);
                return redirect('userCode/' . $phone);
            } else {
                $user = new User();
                $user->phone = $phone;
                $user->active = 'pending';
                $user->role = '0';
                $user->avatar = 'default.png';
                $user->password = Hash::make(request('password'));
                if ($country = Country::where(['iso2' => $request->country_iso])->first()) {
                    $user->country_id = $country->id;
                    $user->current_country_id = $country->id;
                    $user->currency = $country->{"currency_ar"};
                    $user->phonekey = $country->phonekey;
                }
                if (setting('v_code_generate') == 'false' && setting('v_code') != NULL) {
                    $user->code = setting('v_code');
                } else {
                    $user->code = generate_code();
                }
                $user->terms_agreed = 'false';
                $user->save();
                $msg = $msg . $user->code;
                $phone = $user->phone;
                $key = $user->phonekey;
                send_mobile_sms($key . $phone, $msg);
                $phone = $user->phone;
                Session::put('phone', $phone);
                return redirect('userCode/' . $phone);
            }
        } else {
            $msg = implode(' , ', $validator->errors()->all());
            return back()->with('errormsg', $msg);
        }
    }

    public function userCode($phone = '')
    {
        return view('user.code', ['phone' => $phone]);
    }

    public function codeVerfication(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'code' => 'required',
        ]);
        if ($validator->passes()) {
            $number = convert2english(request('phone'));
            $phone = phoneValidate($number);
            // if (substr($number, 0, 1) === '0'){
            //     $number = substr($number, 1);
            // }
            // $phone         = preg_replace('/\s+/', '', $number);
            if ($user = User::where(['phone' => $phone, 'code' => $request->code])->first()) {
//                if($user->delegate == 'true'){
//                    return back()->with('errormsg','هذا الحساب مسجل كمندوب بالفعل.');
//                }
                if ($user->code != request('code')) {
                    $msg = 'كود التحقق الذى ادخلتة غير صحيح.';
                    return back()->with('errormsg', 'كود التحقق الذى ادخلتة غير صحيح.');
                }
                Auth::login($user);
                $site_title = setting('site_title');
                $site_percentage = setting('site_percentage');
                $cities = City::all();
                $regions = Region::all();
                $nationalities = Nationality::all();
                $car_types = citcCarTypes::all();
                return view('user.signup', compact('user', 'site_title', 'site_percentage', 'cities', 'regions', 'nationalities', 'car_types'));
            }
            return back()->with('errormsg', 'كود التحقق الذى ادخلتة غير صحيح.');
        } else {
            return back()->with('errormsg', 'يجب ادخال كود التحقق.');
        }
    }

    public function changeRegion($id)
    {
        $region = Region::find($id);
        if (!$region) {
            return response()->json(['key' => 'fail', 'msg' => 'هذه المنطقة غير متاحة']);
        }
        $cities = City::where('region_id', $region->id)->get();
        $view = view("user.cities", compact('cities'))->render();
        return response()->json(['key' => 'success', 'html' => $view]);
    }

    public function signupDelegation(Request $request)
    {
        $this->validate(request(), [
            'fullname' => 'required',
            'phone' => 'required|unique:users',
            'identity_card' => 'required|min:10',
            'email' => 'required|email|unique:users',
            'driver_date_of_birth' => 'required',
            'bank' => 'required',
            'bank_iban_number' => 'required',
            'mandoubaddress' => 'required',
            'personal_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'driving_license'     => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'identity_card_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'car_back'            => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'car_front'           => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($request->reading_terms == 'on') {
            $number = convert2english($request->phone);
            $phone = phoneValidate($number);
            $meta = new User();
            $meta->phone = $phone;
            $meta->name = $request->fullname;
            $meta->national_id = $request->identity_card;
            $meta->email = $request->email;
            $meta->type = 2;
            $meta->active = 0;
            $meta->birthdate = $request->driver_date_of_birth;
            $meta->address = $request->mandoubaddress;
            $meta->latitude = $request->latitude;
            $meta->langitude = $request->langitude;
            $meta->bank = $request->bank;
            $meta->bank_number = $request->bank_iban_number;
            if ($request->hasFile('personal_image')) {
                $file = $request->file('personal_image');
                $imageName = Str::random(10) . '.' . $file->extension();
                $file->move(
                    base_path() . '/public/images/users/', $imageName
                );
                $meta->photo = '/images/users/' . $imageName;
            }

            if ($request->hasFile('identity_card_image')) {
                $file = $request->file('identity_card_image');
                $identity_card_image = Str::random(10) . '.' . $file->extension();
                $file->move(
                    base_path() . '/public/images/users/', $identity_card_image
                );
                $meta->identity_card_image = '/images/users/' . $identity_card_image;
            }

            if ($request->hasFile('driving_license')) {
                $file = $request->file('driving_license');
                $driving_license = Str::random(10) . '.' . $file->extension();
                $file->move(
                    base_path() . '/public/images/users/', $driving_license
                );
                $meta->driving_license = '/images/users/' . $driving_license;
            }

            if ($request->hasFile('car_back')) {
                $file = $request->file('car_back');
                $car_back = Str::random(10) . '.' . $file->extension();
                $file->move(
                    base_path() . '/public/images/users/', $car_back
                );
                $meta->car_back = '/images/users/' . $car_back;
            }

            if ($request->hasFile('car_front')) {
                $file = $request->file('car_front');
                $car_front = Str::random(10) . '.' . $file->extension();
                $file->move(
                    base_path() . '/public/images/users/', $car_front
                );
                $meta->car_front = '/images/users/' . $car_front;
            }


            $meta->save();
            return redirect()->route('signupSucceed')
                ->with('successmsg', 'تم ارسال جميع البيانات بنجاح, سوف تقوم الادارة بمراجعتها واعتماد طلبكم, شكراً.');

        } else {
            return back()->withErrors(['reading_terms' => 'يجب عليك الموافقة علي الشروط المذكورة.']);
        }
    }

    public function signupSucceed()
    {
        $settings = Setting::find(1);
        return view('user.signupSucceed', compact('settings'));
    }

    #allusers page
    public function allUsers()
    {
        $users = User::with('Role')->latest()->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.allusers', compact('users', 'roles', 'countries', 'cities'));
    }

    public function downloadAllUsers()
    {
        return Excel::download(new UsersExport('allusers'), 'AllUsers.xlsx');
    }

    #users page
    public function users()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->orderBy('num_user_orders', 'DESC')->paginate(1000);
        $userscount = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->count();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.users', compact('users', 'roles', 'countries', 'cities', 'userscount'));
    }

    public function activeUsers()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->where('active', 'active')->orderBy('num_user_orders', 'DESC')->paginate(1000);
        $userscount = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->where('active', 'active')->count();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.activeUsers', compact('users', 'roles', 'countries', 'cities', 'userscount'));
    }

    public function pendingUsers()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->where('active', 'pending')->orderBy('num_user_orders', 'DESC')->paginate(1000);
        $userscount = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->where('active', 'pending')->count();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.pendingUsers', compact('users', 'roles', 'countries', 'cities', 'userscount'));
    }

    public function blockedUsers()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->where('active', 'block')->orderBy('num_user_orders', 'DESC')->paginate(1000);
        $userscount = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'false')->where('active', 'block')->count();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.blockedUsers', compact('users', 'roles', 'countries', 'cities', 'userscount'));
    }

    public function downloadClients($active = null)
    {
        if ($active != null) {
            return Excel::download(new UsersExport('clients', $active), 'clients.xlsx');
        } else {
            return Excel::download(new UsersExport('clients'), 'clients.xlsx');
        }
    }

    #users page
    public function providers()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'true')->orderBy('num_delegate_orders', 'DESC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.providers', compact('users', 'roles', 'countries', 'cities'));
    }

    public function activeProviders()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'true')->where('active', 'active')->orderBy('num_delegate_orders', 'DESC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.activeProviders', compact('users', 'roles', 'countries', 'cities'));
    }

    public function pendingProviders()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'true')->where('active', 'pending')->orderBy('num_delegate_orders', 'DESC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.pendingProviders', compact('users', 'roles', 'countries', 'cities'));
    }

    public function blockedProviders()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'true')->where('active', 'block')->orderBy('num_delegate_orders', 'DESC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.blockedProviders', compact('users', 'roles', 'countries', 'cities'));
    }

    public function notcompleteProviders()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'true')->where('bank_iban_number', NULL)->orderBy('num_delegate_orders', 'DESC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.notcompleteProviders', compact('users', 'roles', 'countries', 'cities'));
    }

    public function balanceProviders()
    {
        $users = User::with('Role')->where('role', '=', '0')->where('delegate', '=', 'true')->where('balance', '>', 0)->orderBy('num_delegate_orders', 'DESC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.balanceProviders', compact('users', 'roles', 'countries', 'cities'));
    }


    public function balanceProvider(Request $request)
    {
        Session::flash('success', 'تم .');
        return redirect('admin/balanceProviders');
    }

    public function downloadProviders($active = null)
    {
        if ($active != null) {
            return Excel::download(new UsersExport('providers', $active), 'providers.xlsx');
        } else {
            return Excel::download(new UsersExport('providers'), 'providers.xlsx');
        }
    }

    public function adminUserPayments($id = false)
    {
        if ($id != false) {
            if ($currentuser = User::find($id)) {
                $payments = Payments::with('user', 'seconduser')->where('user_id', '=', $currentuser->id)->orwhere('second_user_id', '=', $currentuser->id)->orderBy('created_at', 'DESC')->get();
                return view('dashboard.users.payments', compact('payments', 'currentuser'));
            }
            return back()->with('success', 'هذا المستخدم غير موجود.');
        }
        return back();
    }

    public function downloadadminUserPayments($user_id = 0)
    {
        return Excel::download(new PaymentsExport($user_id), 'PaymentsExport.xlsx');
    }

    public function supervisiors()
    {
        $users = User::with('Role')->where('role', '>', '0')->where('id', '!=', '144')->orderBy('role', 'ASC')->get();
        $roles = Role::latest()->get();
        $countries = Country::orderBy('iso2', 'ASC')->get();
        $cities = City::orderBy('name_ar', 'ASC')->get();
        return view('dashboard.users.supervisiors', compact('users', 'roles', 'countries', 'cities'));
    }

    public function downloadSupervisiors()
    {
        return Excel::download(new UsersExport('supervisiors'), 'supervisiors.xlsx');
    }

    public function notifications()
    {
        $regions = Region::all();
        return view('dashboard.users.notifications', compact('regions'));
    }

    public function usersMeta()
    {
        $metas = userMeta::orderBy('created_at', 'DESC')->get();
        return view('dashboard.users.usermetas', compact('metas', $metas));
    }

    public function usersMetaNew()
    {
        $metas = userMeta::whereNotIn('status', ['agree', 'refused'])->orwhere('status', NULL)->orderBy('created_at', 'DESC')->get();
        return view('dashboard.users.usermetasNew', compact('metas', $metas));
    }

    public function usersMetaAgreed()
    {
        $metas = userMeta::where('status', 'agree')->orderBy('created_at', 'DESC')->get();
        return view('dashboard.users.usermetasAgreed', compact('metas', $metas));
    }

    public function usersMetaRefused()
    {
        $metas = userMeta::where('status', 'refused')->orderBy('created_at', 'DESC')->get();
        return view('dashboard.users.usermetasRefused', compact('metas', $metas));
    }

    public function userMeta($id = '')
    {
        $usermeta = userMeta::findOrFail($id);
        $usermeta->seen = 'true';
        $usermeta->update();
        return view('dashboard.users.userMeta', compact('usermeta', $usermeta));
    }

    public function editUserMeta($id = '')
    {
        $usermeta = userMeta::findOrFail($id);
        $site_percentage = setting('site_percentage');
        $cities = City::where('region_id', $usermeta->region_id)->get();
        $regions = Region::all();
        $nationalities = Nationality::all();
        $car_types = citcCarTypes::all();
        return view('dashboard.users.editUserMeta', compact('usermeta', 'site_title ', 'site_percentage', 'cities', 'regions', 'nationalities', 'car_types'));
    }

    public function updateUserMeta(Request $request)
    {
        $this->validate(request(), [
            'fullname' => 'required',
            'phone' => 'required',
            'identity_card' => 'required|min:10',
            'nationality_id' => 'required',
            'region_id' => 'required',
            'city_id' => 'required',
            'email' => 'required',
            'driver_date_of_birth' => 'required',
            'bank_iban_number' => 'required',
            'address' => 'required',
            'personal_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
//            'car_type'            => 'required',
            'car_type_citc_id' => 'required',
            // 'car_model'           => 'required',
            'manufacturing_year' => 'required|min:2',
            'driving_license' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'identity_card_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'car_back' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'car_front' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'form_img' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $number = convert2english($request->phone);
        $phone = phoneValidate($number);
        $meta = userMeta::find($request['id']);
        $meta->phone = $phone;
        $meta->fullname = $request->fullname;
        $meta->identity_card = $request->identity_card;
        $meta->email = $request->email;
        $meta->nationality_id = $request->nationality_id;
        $meta->driver_date_of_birth = $request->driver_date_of_birth;
        $meta->nationalExpireDate = $request->nationalExpireDate;
        $meta->sponsor_name = $request->sponsor_name;
        $meta->region_id = $request->region_id;
        $meta->city_id = $request->city_id;
        $meta->address = $request->address;
        // $meta->bank_acc_number    = $request->bank_acc_number;
        $meta->bank_iban_number = $request->bank_iban_number;
        $meta->car_type = $request->car_type;
        $meta->car_model = $request->car_model;
        $meta->manufacturing_year = $request->manufacturing_year;
        $meta->car_numbers = $request->car_numbers;
        $meta->car_letters = $request->car_letters;
        $meta->car_type_citc_id = $request->car_type_citc_id;
        if ($request->hasFile('personal_image')) {
            $image = $request->file('personal_image');
            $name = md5($request->file('personal_image')->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/user');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $meta->personal_image = $name;
        }
        if ($request->hasFile('driving_license')) {
            $image = $request->file('driving_license');
            $name = md5($request->file('driving_license')->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/user');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $meta->driving_license = $name;
        }
        if ($request->hasFile('identity_card_image')) {
            $image = $request->file('identity_card_image');
            $name = md5($request->file('identity_card_image')->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/user');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $meta->identity_card_image = $name;
        }
        if ($request->hasFile('car_back')) {
            $image = $request->file('car_back');
            $name = md5($request->file('car_back')->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/user');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $meta->car_back = $name;
        }
        if ($request->hasFile('form_img')) {
            $image = $request->file('form_img');
            $name = md5($request->file('form_img')->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/user');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $meta->form_img = $name;
        }
        if ($request->hasFile('car_front')) {
            $image = $request->file('car_front');
            $name = md5($request->file('car_front')->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/img/user');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $meta->car_front = $name;
        }
        $meta->update();
        return back()->with('success', 'تم التعديل بنجاح.');
    }

    public function agreeUserMeta(Request $request)
    {
        $userMeta = userMeta::findOrFail($request->id);
        $userMeta->status = 'agree';
        $userMeta->save();
        if ($user = User::where('phone', '=', $userMeta->phone)->first()) {
            $userMeta->user_id = $user->id;
            $userMeta->save();

            $user->delegate = 'true';
            $user->bank_iban_number = $userMeta->bank_iban_number;
            $user->city_id = $userMeta->city_id;
            /*send notification to provider with admin agree*/
            $devices = userDevices::where(['user_id' => $user->id])->get();
            $notify_title = setting('site_title');
            $message_ar = 'تم الموافقة على طلبك للعمل كمندوب.';
            $message_en = 'Your application has been approved as a delegate.';
            $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'activeDelegate'];
            sendNotification($devices, $message_ar, $notify_title, $data);
            notify($user->id, '', 'user.activeDelegate', "user_id:" . $user->id, '', 'activeDelegate');

            /* end of send FCM notification */
            $user->save();
        } else {
            //create new user account with that data
            $user = new User();
            $user->phone = $userMeta->phone;
            $user->phonekey = ($userMeta->city) ? (($userMeta->city->country) ? $userMeta->city->country->phonekey : '00966') : '00966';
            $user->password = Hash::make(123456);
            $user->name = $userMeta->fullname;
            $user->email = $userMeta->email;
            $user->country_id = ($userMeta->city) ? $userMeta->city->country_id : '1';
            $user->delegate = 'true';
            $user->bank_iban_number = $userMeta->bank_iban_number;
            $user->city_id = $userMeta->city_id;
            $user->active = 'active';
            $user->save();

            $userMeta->user_id = $user->id;
            $userMeta->save();
            send_mobile_sms($user->phonekey . $user->phone, 'تم الموافقة علي طلبك للعمل كعميل قم بالدخول علي التطبيق برقم الهاتف المرسل وكلمة المرور (123456).');
        }
        Session::flash('success', 'تم قبول الطلب بنجاح.');
        return redirect('admin/usersMeta');
    }

    #delete mesage
    public function deleteUserMeta(Request $request)
    {
        userMeta::findOrFail($request->id)->delete();
        Session::flash('success', 'تم حذف الطلب ');
        return redirect('admin/usersMeta');
    }

    #add user
    public function AddUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:190',
            // 'email'    =>'required|email|unique:users',
            'phone' => 'required|min:9|max:190|unique:users',
            'avatar' => 'nullable|image',
            'password' => 'required|min:6|max:190',
            'role' => 'required'
        ]);
        $number = convert2english(request('phone'));
        $phone = phoneValidate($number);
        // if (substr($number, 0, 1) === '0'){
        //     $number = substr($number, 1);
        // }
        // $phone         = preg_replace('/\s+/', '', $number);
        if ($existsuser = User::where(['phone' => $phone])->first()) {
            return back()->with('danger', 'هذا الجوال مستخدم بالفعل.');
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $phone;
        $user->country_id = $request->country_id;
        $user->city_id = $request->city_id;
        $user->role = $request->role;
        if ($request->has('delegate')) {
            $user->delegate = ($request->delegate == 'on') ? 'true' : 'false';
        } else {
            $user->delegate = 'false';
        }
        if ($request->has('balance')) {
            $user->balance = $request->balance;
        } else {
            $user->balance = '0';
        }
        if ($request->has('bank_iban_number')) {
            $user->bank_iban_number = $request->bank_iban_number;
        }
        $user->active = $request->active;
        $user->password = bcrypt($request->password);
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $extension = $file->getClientOriginalExtension();
            $img_extensions = array("jpg", "jpeg", "gif", "png", "svg");
            if (in_array($extension, $img_extensions)) {
                $filename = md5($file->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $extension;
                $file->move(public_path('/img/user/'), $filename);
                $user->avatar = $filename;
            } else {
                return back()->with('danger', 'نوع الصورة التي ادخلتها غير صحيح, الأنواع المسموح بها [gif|jpg|jpeg|png|svg]');
            }
        }
        if (setting('v_code_generate') == 'false' && setting('v_code') != NULL) {
            $user->code = setting('v_code');
        } else {
            $user->code = generate_code();
        }
        $user->save();
        History(Auth::user()->id, 'بأضافة عضو جديد');
        return back()->with('success', 'تم اضافة العضو');
    }

    #update user
    public function UpdateUser(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer',
            'edit_name' => 'required|min:2|max:190',
            // 'edit_email' =>'required|email|min:2|max:190|unique:users,email,'.$request->id,
            'edit_phone' => 'required|min:9|max:190|unique:users,phone,' . $request->id,
            'edit_avatar' => 'nullable|image',
            'edit_role' => 'required'
        ]);
        $number = convert2english(request('edit_phone'));
        $phone = phoneValidate($number);
        // if (substr($number, 0, 1) === '0'){
        //     $number = substr($number, 1);
        // }
        // $phone         = preg_replace('/\s+/', '', $number);
        if ($existsuser = User::where('id', '!=', $request->id)->where('phone', '=', $phone)->first()) {
            return back()->with('danger', 'هذا الجوال مستخدم بالفعل.');
        }
        $user = User::findOrFail($request->id);
        $firstmsg = 'بتعديل ';
        $msg = '';
        if (($request->has('edit_name')) && ($request->edit_name != $user->name)) {
            $msg .= 'الاسم من ' . $user->name . ' الي ' . $request->edit_name . '<br/>';
            $user->name = $request->edit_name;
        }
        if (($request->has('edit_phone')) && ($user->phone != $phone)) {
            $msg .= 'الهاتف من 0' . $user->phone . ' الي 0' . $phone . '<br/>';
            $user->phone = $phone;
        }
        if (($request->has('edit_email')) && ($request->edit_email != $user->email)) {
            $msg .= 'البريد الالكتروني من ' . $user->email . ' الي ' . $request->edit_email . '<br/>';
            $user->email = $request->edit_email;
        }
        if ($request->edit_password) {
            $msg .= 'كلمة المرور الي ' . $request->edit_password . '<br/>';
            $user->password = Hash::make($request->edit_password);
        }
        $user->role = $request->edit_role;
        $user->country_id = $request->edit_country_id;
        $user->city_id = $request->edit_city_id;
        if (($request->has('edit_balance')) && ($request->edit_balance != $user->balance)) {
            $msg .= 'الرصيد من ' . $user->balance . ' الي ' . $request->edit_balance . '<br/>';
            $user->balance = $request->edit_balance;
        }
        if (($request->has('edit_bank_iban_number')) && ($request->edit_bank_iban_number != $user->bank_iban_number)) {
            $msg .= 'رقم الأيبان من ' . $user->bank_iban_number . ' الي ' . $request->edit_bank_iban_number . '<br/>';
            $user->bank_iban_number = $request['edit_bank_iban_number'];
        }
        if (($request->has('edit_active')) && ($request->edit_active != $user->active)) {
            $msg .= 'الحالة من ' . $user->active . ' الي ' . $request->edit_active . '<br/>';
            $user->active = $request->edit_active;
            if ($request->edit_active == 'block') {
                /*send notification to mobile with user delete*/
                $devices = userDevices::where(['user_id' => $user->id])->get();
                $notify_title = setting('site_title');
                $message_ar = 'تم حظر الحساب من تطبيق ' . setting('site_title');
                $message_en = 'Account blocked from app ' . setting('site_title');
                $data = ['title' => $notify_title, 'message_ar' => $message_ar, 'message_en' => $message_en, 'key' => 'block_user'];
                sendNotification($devices, $message_ar, $notify_title, $data);
                /* end of send FCM notification */
                History(Auth::user()->id, 'بحظر العضو ' . $user->name);
            }
        }
        $delegate = ($request->edit_delegate == 'on') ? 'true' : 'false';
        if ($delegate != $user->delegate) {
            $type = ($request->edit_delegate == 'on') ? 'كمندوب' : 'كعميل';
            $msg .= 'للعمل ' . $type;
            $user->delegate = $delegate;
        }
        if ($request->hasFile('edit_avatar')) {
            $file = $request->file('edit_avatar');
            $extension = $file->getClientOriginalExtension();
            $img_extensions = array("jpg", "jpeg", "gif", "png", "svg");
            if (in_array($extension, $img_extensions)) {
                $filename = md5($file->getClientOriginalName()) . time() . rand(99999, 1000000) . '.' . $extension;
                $file->move(public_path('/img/user/'), $filename);
                $user->avatar = $filename;
            } else {
                return back()->with('danger', 'نوع الصورة التي ادخلتها غير صحيح, الأنواع المسموح بها [gif|jpg|jpeg|png|svg]');
            }
        }
        $user->save();
        if ($msg) {
            History(Auth::user()->id, $firstmsg . $msg . ' للمستخدم ' . $user->name);
        }
        return back()->with('success', 'تم حفظ التعديلات');
    }

    #delete user
    public function deleteUser(Request $request)
    {

        $user = User::findOrFail($request->id);
        if (Auth::user()->id == $user->id) {
            Session::flash('danger', 'لا يمكن حذف هذا العضو');
            return 0;
        } else {
            if ($user->avatar != 'default.png') {
                File::delete('img/user/' . $user->avatar);
            }
            $user->delete();
            /*send notification to mobile with user delete*/
            $devices = userDevices::where(['user_id' => $user->id])->get();
            $notify_title = setting('site_title');
            $message_ar = 'تم حذف الحساب من تطبيق ' . setting('site_title');
            $message_en = 'Account deleted from app ' . setting('site_title');
            $data = ['title' => $notify_title, 'message_ar' => $message_ar, 'message_en' => $message_en, 'key' => 'delete_user'];
            sendNotification($devices, $message_ar, $notify_title, $data);
            /* end of send FCM notification */
            History(Auth::user()->id, 'بحذف العضو ' . $user->name);
            return 1;
        }
    }

    #delete user
    public function deleteUsers(Request $request)
    {
        $this->validate(Request(), ['deleteids' => 'required']);
        foreach ($request->deleteids as $id) {
            if ($user = User::find($id)) {
                if ($user->role == 1 || Auth::user()->id == $user->id) {
                    continue;
                }
                if ($user->avatar != 'default.png') {
                    File::delete('img/user/' . $user->avatar);
                }
                $user->delete();
                /*send notification to mobile with user delete*/
                $devices = userDevices::where(['user_id' => $user->id])->get();
                $notify_title = setting('site_title');
                $message_ar = 'تم حذف الحساب من تطبيق ' . setting('site_title');
                $message_en = 'Account deleted from app ' . setting('site_title');
                $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'delete_user'];
                sendNotification($devices, $message_ar, $notify_title, $data);
                /* end of send FCM notification */
            }
        }
        History(Auth::user()->id, 'بحذف أكثر من عضو ');
        return back()->with('success', 'تم الحذف');
    }

    public function comments($id = false)
    {
        if ($id != false) {
            if ($user = User::find($id)) {
                $comments = Comments::where('profile_id', '=', $user->id)->orderBy('created_at', 'DESC')->paginate(30);
                return view('dashboard.users.comments', compact('comments', 'user'));
            }
            return back()->with('success', 'هذا المستخدم غير موجود.');
        }
        return back();
    }

    public function deleteComment(Request $request)
    {
        $comment = Comments::findOrFail($request->id);
        if ($user = User::find($comment->profile_id)) {
            $user->num_comments -= 1;
            $user->save();
        }
        $comment->delete();
        return back()->with('success', 'تم حذف التعليق بنجاح');
    }

    #sms correspondent for all users
    public function SmsMessageAll(Request $request)
    {
        $this->validate($request, [
            'sms_message' => 'required'
        ]);
        $users = User::where('role', '=', '0')->get();
        $numbers = '';
        foreach ($users as $u) {
            $numbers .= $u->phonekey . '' . $u->phone . ',';
        }
        send_mobile_sms($numbers, $request->sms_message);
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    public function SmsMessageSupervisiors(Request $request)
    {
        $this->validate($request, [
            'sms_message' => 'required'
        ]);
        $users = User::where('role', '>', '0')->get();
        $numbers = '';
        foreach ($users as $u) {
            $numbers .= $u->phonekey . '' . $u->phone . ',';
        }
        send_mobile_sms($numbers, $request->sms_message);
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    #sms correspondent for users
    public function SmsMessageClients(Request $request)
    {
        $this->validate($request, [
            'sms_message' => 'required'
        ]);
        $users = User::where('role', '=', '0')->where('delegate', '=', 'false')->get();
        $numbers = '';
        foreach ($users as $u) {
            $numbers .= $u->phonekey . '' . $u->phone . ',';
        }
        send_mobile_sms($numbers, $request->sms_message);
        return back()->with('success', 'تم الارسال بنجاح.');

    }

    #sms correspondent for providers
    public function SmsMessageProviders(Request $request)
    {
        $this->validate($request, [
            'sms_message' => 'required'
        ]);
        $users = User::where('role', '=', '0')->where('delegate', '=', 'true')->get();
        $numbers = '';
        foreach ($users as $u) {
            $numbers .= $u->phonekey . '' . $u->phone . ',';
        }
        send_mobile_sms($numbers, $request->sms_message);
        return back()->with('success', 'تم الارسال بنجاح.');

    }

    #notification correspondent for all users
    public function notificationAllUsers(Request $request)
    {
        $this->validate($request, [
            'notification_message' => 'required'
        ]);
        $devices = DB::table('user_devices')->join('users', 'users.id', '=', 'user_devices.user_id')
            ->where('users.role', '=', '0')
            ->select('user_devices.device_id', 'user_devices.device_type', 'user_devices.show_ads', 'users.id')
            ->get();
        #use FCM or One Signal Here :)
        $notify_title = setting('site_title');
        $message_ar = $request->notification_message;
        $message_en = $request->notification_message;
        $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'from_admin'];
        sendNotification($devices, $message_ar, $notify_title, $data);
        foreach ($devices as $device) {
            $notifications[] = ['user_id' => $device->id,
                'notifier_id' => '',
                'message' => $message_ar,
                'data' => 'user_id:' . $device->id,
                'order_status' => '',
                'key' => 'from_admin',
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        $result = array_unique($notifications, SORT_REGULAR);
        Notifications::insert($result);
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    #notification correspondent for clients
    public function notificationClients(Request $request)
    {
        $this->validate($request, [
            'notification_message' => 'required'
        ]);

        $notifications = [];

        $devices = DB::table('user_devices')
            ->join('users', 'users.id', '=', 'user_devices.user_id')
            ->join('city', 'city.id', '=', 'users.city_id')
            ->join('regions', 'regions.id', '=', 'city.region_id')
            ->where('users.role', '=', '0')
            ->where('users.delegate', '=', 'false')
            ->where(function ($query) use ($request) {
                if ($request['active']) {
                    $query->where('users.active', '=', $request['active']);
                }
                if ($request['device_type']) {
                    $query->where('user_devices.device_type', '=', $request['device_type']);
                }
                if ($request['region_id']) {
                    $query->where('regions.id', '=', $request['region_id']);
                }
            })
            ->select('user_devices.device_id', 'user_devices.device_type', 'users.id')
            ->get();


        #use FCM or One Signal Here :)
        $notify_title = setting('site_title');
        $message_ar = $request->notification_message;
        $message_en = $request->notification_message;
        $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'from_admin'];
        sendNotification($devices, $message_ar, $notify_title, $data);
        foreach ($devices as $device) {
            $notifications[] = ['user_id' => $device->id,
                'notifier_id' => '',
                'message' => $message_ar,
                'data' => 'user_id:' . $device->id,
                'order_status' => '',
                'key' => 'from_admin',
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        $result = array_unique($notifications, SORT_REGULAR);
        Notifications::insert($result);
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    #notification correspondent for all users
    public function notificationProviders(Request $request)
    {
        $this->validate($request, [
            'notification_message' => 'required'
        ]);

        $notifications = [];

        $devices = DB::table('user_devices')
            ->join('users', 'users.id', '=', 'user_devices.user_id')
            ->join('city', 'city.id', '=', 'users.city_id')
            ->join('regions', 'regions.id', '=', 'city.region_id')
            ->where('users.role', '=', '0')
            ->where('users.delegate', '=', 'true')
            ->where(function ($query) use ($request) {
                if ($request['active']) {
                    if ($request['active'] == 'balance') {
                        $query->where('users.bank_iban_number', NULL);
                    } elseif ($request['active'] == 'notcomplete') {
                        $query->where('users.balance', '>', 0);
                    } else {
                        $query->where('users.active', '=', $request['active']);
                    }
                }
                if ($request['device_type']) {
                    $query->where('user_devices.device_type', '=', $request['device_type']);
                }
                if ($request['region_id']) {
                    $query->where('regions.id', '=', $request['region_id']);
                }
            })
            ->select('user_devices.device_id', 'user_devices.device_type', 'users.id')
            ->get();

        #use FCM or One Signal Here :)
        $notify_title = setting('site_title');
        $message_ar = $request->notification_message;
        $message_en = $request->notification_message;
        $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'from_admin'];
        sendNotification($devices, $message_ar, $notify_title, $data);
        foreach ($devices as $device) {
            $notifications[] = ['user_id' => $device->id,
                'notifier_id' => '',
                'message' => $message_ar,
                'data' => 'user_id:' . $device->id,
                'order_status' => '',
                'key' => 'from_admin',
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        $result = array_unique($notifications, SORT_REGULAR);
        Notifications::insert($result);
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    public function notificationSupervisiors(Request $request)
    {
        $this->validate($request, [
            'notification_message' => 'required'
        ]);
        $devices = DB::table('user_devices')->join('users', 'users.id', '=', 'user_devices.user_id')
            ->where('users.role', '>', '0')
            ->select('user_devices.device_id', 'user_devices.device_type', 'user_devices.show_ads', 'users.id')
            ->get();
        #use FCM or One Signal Here :)
        $notify_title = setting('site_title');
        $message_ar = $request->notification_message;
        $message_en = $request->notification_message;
        $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'from_admin'];
        sendNotification($devices, $message_ar, $notify_title, $data);
        foreach ($devices as $device) {
            $notifications[] = ['user_id' => $device->id,
                'notifier_id' => '',
                'message' => $message_ar,
                'data' => 'user_id:' . $device->id,
                'order_status' => '',
                'key' => 'from_admin',
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        $result = array_unique($notifications, SORT_REGULAR);
        Notifications::insert($result);
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    #send sms for current user
    public function currentUserSms(Request $request)
    {
        $this->validate($request, [
            'sms_message' => 'required',
            'phone' => 'required'
        ]);
        if ($user = User::where(['phone' => $request->phone])->first()) {
            send_mobile_sms($user->phonekey . $user->phone, $request->sms_message);
        }
        return back()->with('success', 'تم الارسال بنجاح.');
    }

    #send notification for current user
    public function currentUserNotification(Request $request)
    {
        $this->validate($request, [
            'notification_message' => 'required',
            'user_id' => 'required'
        ]);
        if ($user = User::find($request->user_id)) {
            $devices = userDevices::where(['user_id' => $user->id])->get();
            #use FCM or One Signal Here :)
            $notify_title = setting('site_title');
            $message_ar = $request->notification_message;
            $message_en = $request->notification_message;
            $data = ['title' => $notify_title, 'message_en' => $message_en, 'message_ar' => $message_ar, 'key' => 'from_admin'];
            sendNotification($devices, $message_ar, $notify_title, $data);
            notify($user->id, '', $message_ar, 'user_id:' . $user->id, '', 'from_admin');
            return back()->with('success', 'تم الارسال بنجاح.');
        }

    }

    public function adminAddCoupon(Request $request)
    {
        $this->validate($request, [
            'coupon' => 'required',
            'user_id' => 'required'
        ]);
        if ($coupon = Coupons::where(['code' => $request->coupon])->first()) {
            if ($coupon->num_to_use <= $coupon->num_used) {
                return back()->with('danger', 'لم يعد هذا الكوبون صالح للاستخدام.');
            }
            if (strtotime($coupon->end_at) < strtotime('now')) {
                return back()->with('danger', 'لم يعد هذا الكوبون صالح للاستخدام.');
            }
            $user = User::find($request->user_id);
            if ($usercoupon = usersCoupons::where(['user_id' => $user->id, 'used' => 'false'])->where('end_at', '>=', date('Y-m-d'))->first()) {
                return back()->with('danger', 'لدية كوبون لم يستخدم بالفعل.');
            } else {
                $usercoupon = new usersCoupons();
                $usercoupon->coupon_id = $coupon->id;
                $usercoupon->user_id = $user->id;
                $usercoupon->end_at = $coupon->end_at;
                $usercoupon->save();
                $coupon->num_used += 1;
                $coupon->save();
                History(Auth::user()->id, 'بأضافة كوبون الي ' . $user->name);
                return back()->with('success', 'تم اضافة الكوبون للعميل بنجاح.');
            }
        }
        return back()->with('danger', 'لا يوجد كوبون بهذا الكود.');
    }

    #sms correspondent for all users
    public function admincreateBlock(Request $request)
    {
        $this->validate($request, [
            'num_hours' => 'required',
            'user_id' => 'required'
        ]);
        if ($user = User::find($request->user_id)) {
            if ($block = userBlocks::where('user_id', '=', $request->user_id)->first()) {
                $block->num_hours = $request->num_hours;
                $block->datetime = date('Y-m-d H:i:s');
                $block->to_time = date('Y-m-d H:i:s', strtotime('+' . $request->num_hours . ' hours', strtotime(date('Y-m-d H:i:s'))));
                $block->save();
            } else {
                $block = new userBlocks();
                $block->user_id = $user->id;
                $block->num_hours = $request->num_hours;
                $block->datetime = date('Y-m-d H:i:s');
                $block->to_time = date('Y-m-d H:i:s', strtotime('+' . $request->num_hours . ' hours', strtotime(date('Y-m-d H:i:s'))));
                $block->save();
            }
            History(Auth::user()->id, 'بحظر طلبات ' . $user->name);
            return back()->with('success', 'تم الحظر بنجاح.');
        }
        return back()->with('danger', 'هذا المستخدم غير موجود.');
    }

    public function admincancelBlock(Request $request)
    {
        userBlocks::where('user_id', '=', $request->user_id)->delete();
        return back()->with('success', 'تم انهاء الحظر بنجاح.');
    }

}
