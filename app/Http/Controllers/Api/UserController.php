<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\CompleteProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\UserProfileResource;
use App\Models\Notification;
use App\User;
use Carbon\Carbon;
use Helpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class UserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $user = User::firstOrCreate([
            'phone' => $request->phone,
            'active' => 1
        ]);

        if ($user && $user->active == 1) {
            $code = 1111;
            $user->update([
                'login_code' => $code,
                'code_expire_at' => Carbon::now()->addMinutes(5)
            ]);
//            $messageContent = 'كود التفعيل : ' . $code;
//            \Helpers::send_sms($messageContent, $user->phone);

        } else {
            return response()->json(['error' => 'Your Account Not Activated Yet'], 400);
        }
        return response()->json([
            'user_id' => $user->id,
            'login_code' => $user->login_code,
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login_code' => 'required',
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $login_code = $request->login_code;
        $user_id = $request->user_id;

        $user = User::find($user_id);

        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        if ($user->login_code == $login_code) {
            if (Carbon::now() > $user->code_expire_at) {
                if (request()->header('Accept-Language') == 'ar')
                    $message = 'الكود منتهي .. برجاء الحصول على كود جديد';
                else
                    $message = 'expired login code, get new one';
                return response()->json(['error' => $message], 400);
            }

            if ($user->tokens == null) {
                $access_token = Helpers::generateRandomString();
                $user->update([
                    'tokens' => $access_token,
                ]);
            }
            $user->update([
                'device_token' => $request->fcm_token
            ]);

            return response()->json([
                'complete_data' => $user->name != null ? true : false,
                'user_name' => $user->name ? $user->name : '',
                'type' => $user->type,
                'user_image' => $user->photo ? Helpers::base_url() . '/' . $user->photo : '',
                'joined_from' => $user->created_at ? $user->created_at->toDateString() : '',
                'access_token' => $user->tokens,
            ], 200);
        } else {
            if (request()->header('Accept-Language') == 'ar')
                $message = 'الكود غير صالح';
            else
                $message = 'Invalid login code';
            return response()->json(['error' => $message], 400);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function resendUserActivationCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find($request->user_id);

        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }

//        $code = rand(1111, 9999);
        $code = 1111;
        $user->update([
            'login_code' => $code,
            'code_expire_at' => Carbon::now()->addMinutes(5)
        ]);

//        $messageContent = 'كود التفعيل : ' . $code;
//        \Helpers::send_sms($messageContent, $user->phone);
        $user->save();

        return response()->json([
            'user_id' => $user->id,
            'login_code' => $code,
        ], 200);
    }

    /**
     * @return JsonResponse
     */
    public function getProfile()
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }

        return response()->json(new UserProfileResource($user), 200);
    }

    /**
     * @param Request $request
     * @return mixed|void
     */
    public function changeProfilePicture(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png,gif|required|image',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();

        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $imageName = Str::random(10) . '.' . $file->extension();
            $file->move(
                base_path() . '/public/images/users/', $imageName
            );
            $user->photo = '/images/users/' . $imageName;
            $user->save();
        }
        return response()->json(['image' => \Helpers::base_url() . '/' . $user->photo], 200);
    }


    /**
     * @return JsonResponse
     */

    /**
     * @param UpdateProfileRequest $request
     * @return JsonResponse
     */
    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        if ($user->email != $request->email) {
            $old_email = User::where('email', $request->email)->exists();
            if ($old_email) {
                return response()->json(['error' => 'Email Already Exist'], 400);

            }
        }
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
            'birthyear' => $request->birthyear,
        ]);
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $imageName = Str::random(10) . '.' . $file->extension();
            $file->move(
                base_path() . '/public/images/users/', $imageName
            );
            $user->photo = '/images/users/' . $imageName;
            $user->save();

        }

        return response()->json([], 204);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateLocation(Request $request)
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $user->update([
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
        ]);
        return response()->json([], 204);
    }

    /**
     * @return JsonResponse
     */
    public function getNotifications()
    {
        $user = Helpers::getLoggedUser();
        $notifications = Notification::where('notifiable_id', $user->id)
            ->latest()
            ->select('id', 'data', 'read_at', 'created_at')
            ->get();
        foreach ($notifications as $notification) {
            $notification->data = json_decode($notification->data);
        }
        $user->unreadNotifications->markAsRead();
        return response()->json($notifications, 200);
    }

    public function notifiable()
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        if ($user->notifiable == 1) {
            $user->update(['notifiable' => 0]);
        } else {
            $user->update(['notifiable' => 1]);

        }
        return response()->json([
            'notifiable' => $user->notifiable
        ], 200);
    }

    /**
     * @return JsonResponse
     */
    public function deleteNotification()
    {
        $user = Helpers::getLoggedUser();
        Notification::where('notifiable_id', $user->id)->delete();
        return response()->json([], 204);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function deleteOneNotification($id)
    {

        $note = Notification::where('id', $id)->first();
        if ($note) {
            $note->delete();
            return response()->json([], 204);
        } else {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
    }

    /**
     * @return JsonResponse
     */
    public function unreadNotifications()
    {
        $user = Helpers::getLoggedUser();
        $count = $user->unreadNotifications->count();
        return response()->json(['count' => $count], 200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserFCMToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'fcm_token' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $user->update([
            'device_token' => $request->fcm_token
        ]);
        return response()->json([], 204);
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        $user = Helpers::getLoggedUser();
        if (!$user) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $user->update([
            'device_token' => null
        ]);
        return response()->json([], 204);
    }
}
