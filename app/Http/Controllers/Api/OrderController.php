<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\RateRequest;
use App\Http\Requests\ShippedOrderRequest;
use App\Http\Requests\SpecialOrderRequest;
use App\Http\Resources\Order as OrderResource;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\PendingOrder;
use App\Http\Resources\SavedAddressCollection;
use App\Models\Captain_rate;
use App\Models\Coupon;
use App\Models\Offer;
use App\Models\Order;
use App\Models\order_image;
use App\Models\Portfolio;
use App\Models\SavedAdress;
use App\Models\Setting;
use App\Notifications\NewOrderDeliveryNotification;
use App\Notifications\NewSpecialOrderDeliveryNotification;
use App\User;
use Carbon\Carbon;
use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    /**
     * @param FixOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addOrder(OrderRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $order = Order::create([
            'user_id' => $user->id,
//            'store_id' => $request->store_id,
//            'address_id' => $request->address_id,
            'place_id' => $request->place_id,
            'delivary_point_latitude' => $request->delivary_point_latitude,
            'delivary_point_langitude' => $request->delivary_point_langitude,
            'delivary_time' => $request->delivary_time,
            'delivary_address' => $request->delivary_address,
            'details' => $request->details,
            'payment_type' => $request->payment_type,
//            'coupon_id' => $request->coupon_id?$request->coupon_id:'',
            'status' => 'Pending',
            'order_number' => hexdec(uniqid()),
        ]);
        if ($request->address_id != 0) {
            $order->update(['address_id' => $request->address_id]);
            $address = SavedAdress::find($request->address_id);
            $order->update([
                'delivary_point_latitude' => $address->latitude,
                'delivary_point_langitude' => $address->langitude,
                'delivary_address' => $address->address,
            ]);
        }
        if ($request->store_id != 0) {
            $order->update(['store_id' => $request->store_id]);
        }
        if ($request->coupon_id != 0) {
            $order->update(['coupon_id' => $request->coupon_id]);
        }

        if ($request->images) {
            foreach ($request->images as $image) {

                $imageName = Str::random(10) . '.' . $image->extension();
                $image->move(
                    base_path() . '/public/images/orders/', $imageName
                );
                order_image::create([
                    'order_id' => $order->id,
                    'image' => '/images/orders/' . $imageName
                ]);
            }
        }
        if ($request->address_id == 0) {
            $validator = Validator::make($request->all(), [
                'address_name' => 'required',
                'delivary_point_latitude' => 'required',
                'delivary_point_langitude' => 'required',
                'delivary_address' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], 400);
            }
            SavedAdress::create([
                'user_id' => $user->id,
                'name' => $request->address_name,
                'latitude' => $request->delivary_point_latitude,
                'langitude' => $request->delivary_point_langitude,
                'address' => $request->delivary_address,
            ]);
        }


        // deliveries in the same place

        $deliveries = Helpers::getNearestDeliveries($order->id);
        foreach ($deliveries as $delivery) {
            $near_delivery = User::where('id', $delivery->id)->where('notifiable', 1)->first();

            //notification
            $title = 'تدلل';
            $content = 'طلب وارد';
            $message = [
                'type' => 'order',
                'order_id' => $order->id,
                "order_number" => $order->order_number,
                "delivery_point_latitude" => (double)$order->delivary_point_latitude,
                "delivery_point_longitude" => (double)$order->delivary_point_langitude,
                "delivery_address" => $order->delivary_address,
            ];

            if ($order->place_id == null) {
                $message['store_name'] = Helpers::getLang() == "ar" ? $order->store->name_ar : $order->store->name_en;
                $message["store_logo"] = $order->store->logo ?: "";
                $message["store_latitude"] = (double)$order->store->latitude ?: 0;
                $message['store_longitude'] = (double)$order->store->langitude ?: 0;
                $message['store_address'] = $order->store->address ?: "";
            } else {
                $place_location = Helpers::getPlaceCoordination($order->place_id);
                $message['store_name'] = $place_location['name'];
                $message["store_logo"] = $place_location['icon'];
                $message["store_latitude"] = (double)$place_location['lat'] ?: 0;
                $message['store_longitude'] = (double)$place_location['lng'] ?: 0;
                $message['store_address'] = $place_location['address'] ?: "";
            }
            Notification::send($near_delivery, new NewOrderDeliveryNotification($order));
            \Helpers::fcm_notification($near_delivery->device_token, $content, $title, $message);
        }
        return response()->json(['order' => $order], 200);
    }

    public function addSpecialOrder(SpecialOrderRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }


        $order = Order::create([
            'user_id' => $user->id,
            'type' => 2, // special order
            'delivary_point_latitude' => $request->delivary_point_latitude,
            'delivary_point_langitude' => $request->delivary_point_langitude,
            'delivary_address' => $request->delivary_address,
            'order_point_latitude' => $request->order_point_latitude,
            'order_point_langitude' => $request->order_point_langitude,
            'order_address' => $request->order_address,
            'details' => $request->details,
//            'payment_type' => $request->payment_type,
//            'coupon_id' => $request->coupon_id,
            'status' => 'Pending',
            'order_number' => hexdec(uniqid()),
        ]);

        if ($request->images) {
            foreach ($request->images as $image) {

                $imageName = Str::random(10) . '.' . $image->extension();
                $image->move(
                    base_path() . '/public/images/orders/', $imageName
                );
                order_image::create([
                    'order_id' => $order->id,
                    'image' => '/images/orders/' . $imageName
                ]);
            }
        }

        // deliveries in the same place

        $deliveries = Helpers::getNearestDeliveriesForSpecialOrders($order->id);

        foreach ($deliveries as $delivery) {
            $near_delivery = User::where('id', $delivery->id)->where('notifiable', 1)->first();

            //notification
            $title = 'تدلل';
            $content = 'طلب خاص وارد';
            $message = [
                'type' => 'special_order',
                'order_id' => $order->id,
                "order_number" => $order->order_number,
                "delivery_point_latitude" => (double)$order->delivary_point_latitude,
                "delivery_point_longitude" => (double)$order->delivary_point_langitude,
                "delivery_address" => $order->delivary_address,
                "order_point_latitude" => (double)$order->delivary_point_latitude,
                "order_point_longitude" => (double)$order->order_point_langitude,
                "order_address" => $order->order_address,
                "store_logo" => "",
                "store_name" => "",
            ];
            Notification::send($near_delivery, new NewSpecialOrderDeliveryNotification($order));
            \Helpers::fcm_notification($near_delivery->device_token, $content, $title, $message);
        }
        return response()->json(['order' => $order], 200);
    }

    /**
     * @param ShippedOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addShippedOrder(ShippedOrderRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order = Order::create([
            'user_id' => $user->id,
            'type' => 3, // shipped order
            'delivary_point_latitude' => $request->delivary_point_latitude,
            'delivary_point_langitude' => $request->delivary_point_langitude,
            'delivary_address' => $request->delivary_address,
            'order_point_latitude' => $request->order_point_latitude,
            'order_point_langitude' => $request->order_point_langitude,
            'order_address' => $request->order_address,
            'details' => $request->details,
            'shipping_method' => $request->shipping_method,
            'status' => 'Pending',
            'order_number' => hexdec(uniqid()),
        ]);

        if ($request->images) {
            foreach ($request->images as $image) {
                $imageName = Str::random(10) . '.' . $image->extension();
                $image->move(
                    base_path() . '/public/images/orders/', $imageName
                );
                order_image::create([
                    'order_id' => $order->id,
                    'image' => '/images/orders/' . $imageName
                ]);
            }
        }

        // deliveries in the same place
        $deliveries = Helpers::getNearestDeliveriesForSpecialOrders($order->id);
        foreach ($deliveries as $delivery) {
            $near_delivery = User::where('id', $delivery->id)->where('notifiable', 1)->first();
            //notification
            $title = 'تدلل';
            $content = 'طلب نقل امتعة وارد';
            $message = [
                'type' => 'shipped_order',
                'order_id' => $order->id,
                "order_number" => $order->order_number,
                "delivery_point_latitude" => (double)$order->delivary_point_latitude,
                "delivery_point_longitude" => (double)$order->delivary_point_langitude,
                "delivery_address" => $order->delivary_address,
                "order_point_latitude" => (double)$order->delivary_point_latitude,
                "order_point_longitude" => (double)$order->order_point_langitude,
                "order_address" => $order->order_address,
                "store_logo" => "",
                "store_name" => "",
            ];
            Notification::send($near_delivery, new NewSpecialOrderDeliveryNotification($order));
            \Helpers::fcm_notification($near_delivery->device_token, $content, $title, $message);
        }
        return response()->json(['order' => $order], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'latitude' => 'required',
            'langitude' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        SavedAdress::create([
            'user_id' => $user->id,
            'name' => $request->name,
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
            'address' => $request->address,

        ]);

        return response()->json([], 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activeCoupon(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
//            'store_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $coupon = Coupon::where('code', $request->code)
//            ->where('store_id', $request->store_id)
            ->where('expired_at', '>=', Carbon::now())->first();
        if ($coupon)
            return response()->json(['coupon' => $coupon->id], 200);
        else
            return response()->json([], 404);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSavedAddress()
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $addresses = SavedAdress::where('user_id', $user->id)->get();

        return response()->json(new SavedAddressCollection($addresses), 200);
    }

    public function getPortfolio()
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $portfolios = Portfolio::where('user_id', $user->id)
            ->select(['id', 'debt', 'earning', 'order_id', 'created_at'])
            ->with(['order' => function ($q) {
                $q->select(['id', 'order_number', 'created_at']);

            }])->get();
        $portfolio['debt'] = $portfolios->sum('debt');
        $portfolio['earning'] = $portfolios->sum('earning');


        $portfolio['operations'] = $portfolios;


        return response()->json($portfolio, 200);
    }

    public function deleteSavedAddress($id)
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        SavedAdress::where('id', $id)->delete();

        return response()->json([], 204);
    }

    public function cancelOrder($id)
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        Order::where('id', $id)->update(['status' => 'Canceled']);

        return response()->json([], 204);
    }

    public function changeOrderStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'offer_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = Helpers::getLoggedUser();
        $offer = Offer::find($request->offer_id);
        $order = Order::where('id', $offer->order->id)->first();
        $order->update([
            'status' => $request->status
        ]);
        //portofolio
        $commission = Setting::first()->commission;
        if ($request->status == 'Completed') {
            if ($order->payment_type == 'Cash') {
                Portfolio::create([
                    'debt' => $commission,
                    'user_id' => $offer->captain_id,
                    'order_id' => $order->id
                ]);
            }
        }

        return response()->json([
            'status' => $request->status,
            'user' => [
                'id' => $user->id,
                'type' => $user->type
            ]
        ], 200);
    }

    /**
     * @param RateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        if ($user->type == 1 && $user->role == 2) {
            $orders = Order::where('user_id', $user->id)->whereIn('status', ['InProgress', 'Completed'])->latest()->get();
        } else {
            $order_ids = Offer::where('captain_id', $user->id)
                ->where('status', 'Accepted')->latest()->pluck('order_id')->toArray();
            $ids_ordered = implode(',', array_values($order_ids));
            $orders = Order::whereIn('id', $order_ids)->whereIn('status', ['InProgress', 'Completed'])
                ->orderByRaw("FIELD(id, $ids_ordered)")
                ->get();
        }

        if (!$orders) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        return response()->json(new OrderCollection($orders), 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOrders()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        Order::where('user_id', $user->id)->delete();
        return response()->json([], 204);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderDetails($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order = Order::find($id);

        if (!$order) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        return response()->json(new OrderResource($order), 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderCount($id)
    {
        $user = User::where('id', $id)->select(['id', 'name', 'photo'])->first();
        $user['photo'] = \Helpers::base_url() . '/' . $user->photo;
        $user['order_count'] = Order::where('user_id', $id)->count();
        $user['rates'] = Captain_rate::where('user_id', $id)->count();


        return response()->json(['user' => $user], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPendingOrder($id)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $order = Order::find($id);
        $offers = Offer::where('order_id', $id)->get();
        if ($offers) {
            foreach ($offers as $offer) {
                if ($offer->status == 'Accepted' && $offer->captain_id != $user->id) {
                    $ar_mgs = 'تم قبول الطلب بواسطة مندوب اخر';
                    $en_mgs = 'order has taken by another captain';
                    return response()->json(['error' => Helpers::getLang() == 'ar' ? $ar_mgs : $en_mgs], 400);
                } else {
                    return response()->json(new PendingOrder($order), 200);
                }
            }
        }
        return response()->json(new PendingOrder($order), 200);

    }
}
