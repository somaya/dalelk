<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUs;
use App\Http\Resources\AdvCollection;
use App\Http\Resources\BranchCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CityCollection;
use App\Http\Resources\HomeResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\Setting as SettingResource;
use App\Http\Resources\StateCollection;
use App\Http\Resources\StoreCollection;
use App\Models\Adv;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\State;
use App\Models\Store;
use App\storeMenus;
use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function __construct()
    {

    }

    /**
     * @param $lng
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLanguage($lng)
    {
        $message = session(['locale' => $lng]);
        return response()->json($message, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories()
    {
        $categories = Category::all();
        return response()->json(new CategoryCollection($categories), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStores()
    {
        $lang = (request()->header('lang')) ?? 'ar';
        $cats = 'cafe|restaurant|supermarket|bakery|pharmacy';
        $lat = doubleval(23.8859);
        $long = doubleval(45.0792);
        $googleKey = 'AIzaSyA4G9HchJv7Dy4Z2JCeHJaYvzX1cfbg0-M';

        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" . $lat . "," . $long . "&name=" . $cats . "&opennow=true&rankby=distance&key=" . $googleKey . "&language=" . $lang;

        $jsonresult = file_get_contents($url);
        $results = json_decode($jsonresult);
        if ($results->results) {
            $categories = ['cafe', 'restaurant', 'supermarket', 'bakery', 'pharmacy'];
            foreach ($results->results as $result) {
                $s_photos = [];
                $types = $result->types;
                if (isset($types[0])) {
                    if (in_array($types[0], $categories)) {
                        $icon = url('img/icons/' . $types[0] . '.png');
                    } else {
                        $icon = url('img/icons/store.png');
                    }
                } else {
                    $icon = url('img/icons/store.png');
                }
                $distance = Helpers::directDistance($lat, $long, $result->geometry->location->lat, $result->geometry->location->lng);
                $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";
                foreach ($result->photos as $photos) {
                    $s_photos['image'] = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $photos->width . '&photoreference=' . $photos->photo_reference . '&key=' . $googleKey;
                }
                $nearstores[] = [
                    'name' => $result->name,
                    'lat' => doubleval($result->geometry->location->lat),
                    'lng' => doubleval($result->geometry->location->lng),
                    'icon' => $icon,
                    'image' => $s_photos,
                    'place_id' => $result->place_id,
                    'reference' => $result->reference,
                    'vicinity' => $result->vicinity,
                    'is_open' => $result->opening_hours->open_now,
                    'rate' => $result->rating,
                    'distance' => $distance,
                    'types' => $result->types
                ];
            }
            return response()->json($nearstores, 200);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHome(Request $request)
    {
        $lang = (request()->header('Accept-Language')) ?? 'ar';
        $lat = doubleval($request->lat);
        $long = doubleval($request->lng);
        $googleKey = 'AIzaSyA4G9HchJv7Dy4Z2JCeHJaYvzX1cfbg0-M';
        $cats = 'cafe|restaurant|supermarket|bakery|pharmacy';
        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" . $lat . "," . $long . "&name=" . $cats . "&opennow=true&rankby=distance&key=" . $googleKey . "&language=" . $lang;
        $jsonresult = file_get_contents($url);
        $results = json_decode($jsonresult);
        if ($results->results) {
            $categories = ['cafe', 'restaurant', 'supermarket', 'bakery', 'pharmacy'];
            foreach ($results->results as $result) {
                $types = $result->types;
                if (isset($types[0])) {
                    if (in_array($types[0], $categories)) {
                        $icon = url('img/icons/' . $types[0] . '.png');
                    } else {
                        $icon = url('img/icons/store.png');
                    }
                } else {
                    $icon = url('img/icons/store.png');
                }
                $distance = Helpers::directDistance($lat, $long, $result->geometry->location->lat, $result->geometry->location->lng);
                $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";
                $s_photos = [];
                $place_photos = [];
                if (isset($result->photos)) {
                    foreach ($result->photos as $pic) {
                        $s_photos[] = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $pic->width . '&photoreference=' . $pic->photo_reference . '&key=' . $googleKey;
                    }
                    foreach ($s_photos as $key => $s_photo) {
                        $place_photos[] = $s_photo;
                    }
                }

                $nearstores[] = [
                    'name' => $result->name,
                    'lat' => doubleval($result->geometry->location->lat),
                    'lng' => doubleval($result->geometry->location->lng),
                    'icon' => $icon,
                    'image' => $place_photos,
                    'place_id' => $result->place_id,
                    'vicinity' => $result->vicinity,
                    'is_open' => $result->opening_hours->open_now,
                    'rating' => property_exists($result, 'rating') ? $result->rating : 0,
                    'distance' => $distance,
                    'types' => $result->types
                ];
            }
        } else {
            $nearstores = [];
        }
        $sliders = Slider::all();
        $categories = Category::all();

        return response()->json([
            'sliders' => new AdvCollection($sliders),
            'categories' => new CategoryCollection($categories),
            'stores' => $nearstores
        ], 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlaceDetails(Request $request)
    {
        $googleKey = 'AIzaSyA4G9HchJv7Dy4Z2JCeHJaYvzX1cfbg0-M';
        $lang = (request()->header('lang')) ?? 'ar';
        $url = "https://maps.googleapis.com/maps/api/place/details/json?place_id=" . $request->place_id . "&fields=address_component,adr_address,business_status,formatted_address,geometry,icon,name,photo,place_id,plus_code,type,url,utc_offset,formatted_phone_number,opening_hours,rating,review,user_ratings_total&key=" . $googleKey;
        $json = file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK") {
            $result = $data->result;
            $lat = $request->lat;
            $long = $request->long;
            $distance = Helpers::directDistance($lat, $long, $result->geometry->location->lat, $result->geometry->location->lng);
            $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";
            $categories = ['cafe', 'restaurant', 'supermarket', 'bakery', 'pharmacy'];
            $types = $result->types;
            if (isset($types[0])) {
                if (in_array($types[0], $categories)) {
                    $icon = url('img/icons/' . $types[0] . '.png');
                } else {
                    $icon = url('img/icons/store.png');
                }
            } else {
                $icon = url('img/icons/store.png');
            }
            $s_photos = [];
            $place_photos = [];
            $open_times = [];
            if (isset($result->photos)) {
                foreach ($result->photos as $pic) {
                    $s_photos[] = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $pic->width . '&photoreference=' . $pic->photo_reference . '&key=' . $googleKey;
                }
                foreach ($s_photos as $key => $s_photo) {
                    $place_photos[$key]['image'] = $s_photo;
                }
            }

            foreach ($result->opening_hours->weekday_text as $key => $open_time) {
                $open_times[$key]['time'] = $open_time;
            }
            $my_address = '';
            foreach ($result->address_components as $adr) {
                if ($adr->types[0] == 'postal_code')
                    continue;
                $my_address .= $adr->long_name . ' ';
            }

            $place = [
                'name' => $result->name,
                'lat' => doubleval($result->geometry->location->lat),
                'lng' => doubleval($result->geometry->location->lng),
                'address' => $my_address,
                'icon' => $icon,
                'g_icon' => $result->icon,
                'place_images' => $place_photos,
                'distance' => $distance,
                'phone_number' => isset($result->formatted_phone_number) ? $result->formatted_phone_number : "",
                'opening_hours' => $open_times,
                'rating' => property_exists($result, 'rating') ? $result->rating : 0,
                'reviews' => property_exists($result, 'reviews') ? $result->reviews : [],
                'user_ratings_total' => property_exists($result, 'user_ratings_total') ? $result->user_ratings_total : 0,
                'types' => $result->types
            ];
            return response()->json($place, 200);
        } else {
            return response()->json(['error' => 'No data'], 400);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function getBranches()
    {
        $branches = Branch::all();
        return response()->json(new BranchCollection($branches), 200);

    }

    public
    function getProducts($id)
    {
        $category = Category::find($id);
        if (!$category) {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $products = Product::where('category_id', $id)->orderBy('created_at', 'desc')->paginate(5);
        return response()->json(new ProductCollection($products), 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSettings()
    {
        $settings = Setting::first();
        return response()->json(new SettingResource($settings), 200);

    }


    /**
     * @param ContactUs $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function contactUs(ContactUs $request)
    {
        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }

        Contact::create([
            'user_id' => $user->id,
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message
        ]);
        return response()->json([], 204);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStoresByCategories(Request $request)
    {
        $cat_stores = [];
        $cat = Category::where('name_en', $request->type)->pluck('id');
        $stores = Store::whereIn('category_id', $cat)->get();

        // get stores from google

        $lang = (request()->header('lang')) ?? 'ar';
        $lat = doubleval($request->lat);
        $long = doubleval($request->lng);
        $googleKey = 'AIzaSyA4G9HchJv7Dy4Z2JCeHJaYvzX1cfbg0-M';

        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" . $lat . "," . $long . "&name=" . $request->type . "&opennow=true&rankby=distance&key=" . $googleKey . "&language=" . $lang;
        $jsonresult = file_get_contents($url);
        $results = json_decode($jsonresult);
        if ($results->results) {
            $categories = [$request->type];
            foreach ($results->results as $result) {
                $types = $result->types;
                if (isset($types[0])) {
                    if (in_array($types[0], $categories)) {
                        $icon = url('img/icons/' . $types[0] . '.png');
                    } else {
                        $icon = url('img/icons/store.png');
                    }
                } else {
                    $icon = url('img/icons/store.png');
                }
                $place_photos = [];
                if (isset($result->photos)) {
                    foreach ($result->photos as $pic) {
                        $s_photos[] = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $pic->width . '&photoreference=' . $pic->photo_reference . '&key=' . $googleKey;
                    }
                    foreach ($s_photos as $key => $s_photo) {
                        $place_photos[$key] = $s_photo;
                    }
                }

                $distance = Helpers::directDistance($lat, $long, $result->geometry->location->lat, $result->geometry->location->lng);
                $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";

                $nearstores[] = [
                    'id' => 0,
                    'name' => $result->name,
                    'icon' => $icon,
                    'g_icon' => $result->icon,
                    'place_images' => $place_photos,
                    'address' => $result->vicinity ?: "",
                    'lat' => doubleval($result->geometry->location->lat),
                    'lng' => doubleval($result->geometry->location->lng),
                    'distance' => $distance,
                    'place_id' => $result->place_id,
                ];
            }
        }

        foreach (new StoreCollection($stores) as $store) {
            $cat_stores[] = $store;
        }
        foreach ($nearstores as $nearstore) {
            $cat_stores[] = $nearstore;
        }
        return response()->json($cat_stores, 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchStores(Request $request)
    {
        $lang = ($request->header('Accept-Language')) ?? 'ar';
        $data = [];
        $places = [];
        $specialstores = [];
        $distance = 0;
        $next_page_token = '';
        $opening_hours = [];
        $icon = '';
        $places_ended = false;
        $googlekey = 'AIzaSyA4G9HchJv7Dy4Z2JCeHJaYvzX1cfbg0-M';
        $max_distance = 25;
        $name = $request->name;
        $page = ($request->page) ?? 1;
        $offset = ($page - 1) * 20;//$this->limit
        $msg = '';
        $lat = doubleval($request->lat);
        $long = doubleval($request->long);

//         get nearst  special stores with menus
        if ($page == 1) {
            $stores = DB::select("SELECT * FROM ( SELECT *, ( 6371 * acos( cos( radians('" . $lat . "') ) * cos( radians( latitude ) ) * cos( radians( langitude ) - radians('" . $long . "') ) + sin( radians('" . $lat . "') ) * sin( radians( latitude ) ) ) ) AS distance FROM stores where name_ar LIKE '%$name%' OR name_en LIKE '%$name%' HAVING distance <= " . $max_distance . " ORDER BY distance ASC limit 40 offset 0) as nearstBranchs GROUP BY name_en order By distance ASC limit 20 offset $offset");
            foreach ($stores as $store) {
                $distance = round($store->distance, 2);
                $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";
                $storemenus = StoreMenu::where('store_id', '=', $store->id)->get();
                $menus = [];
                foreach ($storemenus as $menu) {
                    $menus[] = $menu->item;
                }
                $opening_hours = ($store->{"opening_hours_$lang"} == null) ? [] : explode(',', $store->{"opening_hours_$lang"});
                $rate = ($store->num_rating > 0) ? floatval($store->rating / $store->num_rating) : 0.0;
                $specialstores[] = ['id' => $store->id,
                    'name' => ($store->{"name_$lang"}) ?? '',
                    'icon' => url('img/store/icons/' . $store->icon),
                    'cover' => url('img/store/cover/' . $store->cover),
                    'phone' => ($store->phone) ?? '',
                    'email' => ($store->email) ?? '',
                    'address' => ($store->address) ?? '',
                    'lat' => doubleval($store->lat),
                    'long' => doubleval($store->lng),
                    'rate' => $rate,
                    'num_rating' => ($store->num_rating) ?? 0,
                    'num_comments' => ($store->num_comments) ?? 0,
                    'website' => ($store->website) ?? '',
                    'opening_hours' => $opening_hours,
                    'distance' => $distance,
                    'menus' => $menus,
                    'offer' => ($store->offer) ?? 'false',
                    'offer_amount' => '' . ($store->offer_amount) ?? '',
                    'offer_max' => '' . ($store->offer_max) ?? ''

                ];
            }
        }

        $name = urlencode($request->name);
        $next_page_token = ($request->next_page_token == '') ? '' : '&pagetoken=' . $request->next_page_token;
        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" . $lat . "," . $long . "&name=" . $name . "&opennow=true&rankby=distance&key=" . $googlekey . "&language=" . $lang . $next_page_token;
        $jsonresult = file_get_contents($url);
        $results = json_decode($jsonresult);
        if ($results->results) {
            $categories = (setting('place_types') != '') ? explode('|', setting('place_types')) : ['cafe', 'restaurant', 'supermarket', 'bakery', 'pharmacy'];
            foreach ($results->results as $result) {
                $types = $result->types;
                if (isset($types[0])) {
                    if (in_array($types[0], $categories)) {
                        $icon = url('img/icons/' . $types[0] . '.png');
                    } else {
                        $icon = url('img/icons/store.png');
                    }
                } else {
                    $icon = url('img/icons/store.png');
                }

                $distance = directDistance($lat, $long, $result->geometry->location->lat, $result->geometry->location->lng);
                $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";
                if (intval($distance) <= 50) {
                    $places[] = [
                        'name' => $result->name,
                        'lat' => doubleval($result->geometry->location->lat),
                        'lng' => doubleval($result->geometry->location->lng),
                        'icon' => $icon,
                        'place_id' => $result->place_id,
                        'distance' => $distance,
                        'address' => $result->vicinity ?: "",
                    ];
                }

            }
            $next_page_token = (isset($results->next_page_token)) ? $results->next_page_token : '';
            $places_ended = ($next_page_token == '') ? true : false;
        }
        $data = [
            'places' => $places,
        ];
        return response()->json($data, 200);
    }


}


