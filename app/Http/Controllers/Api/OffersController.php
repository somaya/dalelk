<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RateRequest;
use App\Http\Requests\UserRateRequest;
use App\Http\Resources\CaptainRatesCollection;
use App\Http\Resources\OfferCollection;
use App\Http\Resources\Order;
use App\Http\Resources\UserRatesCollection;
use App\Models\Captain_rate;
use App\Models\Offer;
use App\Models\SavedAdress;
use App\Models\User_rates;
use App\Notifications\AcceptOfferDeliveryNotification;
use App\Notifications\NewOfferDeliveryNotification;
use App\Notifications\RefurseOfferDeliveryNotification;
use App\User;
use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OffersController extends Controller
{
    public function acceptOffer($id)
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $offer = Offer::where('id', $id)->first();
        $offer_ids = Offer::where('order_id', $offer->order->id)->pluck('id');
        if (count($offer_ids) > 0) {
            foreach ($offer_ids as $offer_id) {
                $other_offer = Offer::find($offer_id);
                $other_offer->update(['status' => 'Rejected']);
            }
        }
        $offer->update(['status' => 'Accepted']);
        $offer->order->update(['status' => 'InProgress']);
        $captain = User::where('id', $offer->captain_id)->select(['id', 'name', 'photo', 'phone', 'latitude', 'langitude', 'device_token'])->first();
        $captain['delivary_cost'] = $offer->delivary_cost;


        //notification
        $title = 'تدلل';
        $content = 'تقديم عرض سعر';
        $message = [
            'type' => 'accept_offer',
            "offer_id" => (int)$offer->id,
            "user_id" => (int)$offer->order->user_id,
        ];
        Notification::send($offer->captain, new AcceptOfferDeliveryNotification($offer));
        \Helpers::fcm_notification($captain->device_token, $content, $title, $message);


        return response()->json(['order' => new Order($offer->order), 'captain' => $captain, 'offer_id' => $offer->id], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function allOffers($id)
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $offers = Offer::where('order_id', $id)->get();
        return response()->json(['offers' => new OfferCollection($offers)], 200);
    }

    public function rejectOffer($id)
    {

        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        $offer = Offer::find($id);

        $offer->update([
            'status' => 'Rejected'
        ]);

        //notification
        $title = 'تدلل';
        $content = 'رفض عرض سعر';
        $message = [
            'type' => 'reject_offer',
            "offer_id" => (int)$id,
        ];
        Notification::send($offer->captain, new RefurseOfferDeliveryNotification($offer));
        \Helpers::fcm_notification($offer->captain->device_token, $content, $title, $message);

        return response()->json([], 204);
    }

    public function cancelOffer($id)
    {
        if (Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => Helpers::failFindId()], 400);
        }
        Offer::where('id', $id)->delete();

        return response()->json([], 204);
    }

    /**
     * @param RateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rateCaptain(RateRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        Captain_rate::create([
            'user_id' => $user->id,
            'captain_id' => $request->captain_id,
            'degree' => $request->degree,
            'comment' => $request->comment,
        ]);
        return response()->json([], 204);
    }

    /**
     * @param UserRateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function rateUser(UserRateRequest $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        User_rates::create([
            'user_id' => $request->user_id,
            'captain_id' => $user->id,
            'degree' => $request->degree,
            'comment' => $request->comment,

        ]);
        return response()->json([], 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendOffer(Request $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $offer = Offer::create([
            'order_id' => $request->order_id,
            'captain_id' => $user->id,
            'delivary_cost' => $request->delivary_cost,
            'status' => 'Pending',
        ]);

        $rates = User_rates::where('captain_id', $offer->captain_id);
        if ($offer->order->address_id) {
            $address = SavedAdress::find($offer->order->address_id);
            $delivery_point_latitude = (double)$address->latitude;
            $delivery_point_longitude = (double)$address->langitude;
        } else {
            $delivery_point_latitude = (double)$offer->order->delivary_point_latitude;
            $delivery_point_longitude = (double)$offer->order->delivary_point_langitude;
        }
        //notification
        $title = 'تدلل';
        $content = 'تقديم عرض سعر';
        $message = [
            'type' => 'offer',
            "offer_id" => (int)$offer->id,
            "order_id" => (int)$offer->order->id,
            "order_number" => (int)$offer->order->order_number,
            "captain_id" => (int)$offer->captain->id,
            "captain_name" => $offer->captain->name ?: "",
            "captain_image" => Helpers::base_url() . $offer->captain->photo ?: "",
            "captain_rate" => (float)$rates->avg('degree') ?: 0,
            "rate_count" => $rates->count() ?: 0,
            "captain_latitude" => (double)$offer->captain->latitude ?: 0,
            "captain_longitude" => (double)$offer->captain->langitude ?: 0,
            "delivery_point_latitude" => $delivery_point_latitude,
            "delivery_point_longitude" => $delivery_point_longitude,
            "delivery_time" => $offer->order->delivary_time ?: "",
            "delivery_cost" => (double)$offer->delivary_cost ?: 0,
        ];
        Notification::send($offer->order->user, new NewOfferDeliveryNotification($offer));
        \Helpers::fcm_notification($offer->order->user->device_token, $content, $title, $message);

        return response()->json([], 204);
    }

    //تقييمات المناديب للعملاء
    public function getCaptainsRates($id)
    {
        $rates = User_rates::where('user_id', $id)->get();
        return response()->json(['rates' => new UserRatesCollection($rates)], 200);
    }

    //تقييمات المستخدمين للمناديب
    public function getUsersRates($id)
    {
        $rates = Captain_rate::where('captain_id', $id)->get();
        return response()->json(['rates' => new CaptainRatesCollection($rates)], 200);
    }
}
