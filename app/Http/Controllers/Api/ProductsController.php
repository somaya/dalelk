<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccessoriesCollection;
use App\Http\Resources\ChoiceCollection;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection as ProductCollectionResource;
use App\Models\Accessory;
use App\Models\Choice;
use App\Models\Choice_product;
use App\Models\ContractRequest;
use App\Models\Favourite;
use App\Models\Maintenance;
use App\Models\PriceRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{

    public function addChoice(Request $request)
    {

        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'branch_id' => 'required',
            'contract_date' => 'required',
            'payment_date' => 'required',
            'products' => 'required',

        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
       $choice= Choice::create([
            'name'=>$request->name,
            'user_id'=>$user->id,
            'phone'=>$request->phone,
            'branch_id'=>$request->branch_id,
            'payment_date'=>$request->payment_date,
            'contract_date'=>$request->contract_date,
        ]);
        foreach ($request->products as $product)
        {
            Choice_product::create([
                'choice_id' => $choice->id,
                'product_id' => $product,

            ]);
        }

        return response()->json([], 204);

    }
    public function maintenanceRequest(Request $request)
    {

        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'branch_id' => 'required',
            'address' => 'required',
            'details' => 'required',
            'service_type' => 'required',
            'elevator_type' => 'required',
            'elevator_age' => 'required',

        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        Maintenance::create([
            'name'=>$request->name,
            'user_id'=>$user->id,
            'phone'=>$request->phone,
            'branch_id'=>$request->branch_id,
            'address'=>$request->address,
            'service_type'=>$request->service_type,
            'elevator_type'=>$request->elevator_type,
            'elevator_age'=>$request->elevator_age,
            'details'=>$request->details,
        ]);


        return response()->json([], 204);

    }
    public function priceRequest(Request $request)
    {

        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'choice_id' => 'required',
            'address' => 'required',
            'details' => 'required',


        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        PriceRequest::create([
            'name'=>$request->name,
            'user_id'=>$user->id,
            'phone'=>$request->phone,
            'choice_id'=>$request->choice_id,
            'address'=>$request->address,
            'details'=>$request->details,
        ]);


        return response()->json([], 204);

    }
    public function contractRequest(Request $request)
    {

        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'choice_id' => 'required',
            'address' => 'required',
            'national_number' => 'required',


        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }


        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        ContractRequest::create([
            'name'=>$request->name,
            'user_id'=>$user->id,
            'phone'=>$request->phone,
            'choice_id'=>$request->choice_id,
            'address'=>$request->address,
            'national_number'=>$request->national_number,
        ]);


        return response()->json([], 204);

    }
    public function getMyChoices()
    {

        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $choices_ids=Choice::where('user_id',$user->id)->pluck('id')->toArray();
        $choices= Choice_product::whereIn('choice_id',$choices_ids)->with(['product'=>function($q){
            $q->select('id','name','price','photo');
        }])->get();
//        $choices->transform(function($i)  {
//            $i->product->photo=$i->product->photo? \Helpers::base_url().'/' .$i->product->photo:null;
//            return $i;
//        });

        $choices=$choices->groupBy('choice_id')->values();
        return response()->json($choices, 200);


    }
    public function deleteChoice($id)
    {
        Choice_product::where('id',$id)->delete();
        return response()->json([], 204);

    }


}
