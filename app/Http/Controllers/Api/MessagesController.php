<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\Invoice;
use App\Models\Message;
use App\Models\Offer;
use App\Models\Setting;
use App\Notifications\NewInvoiceNotification;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class MessagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'to_user_id' => 'required',
            'offer_id' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $to_user = User::where('id', $request->to_user_id)->first();

        $message = Message::create([
            'from_user_id' => $user->id,
            'to_user_id' => $to_user->id,
            'message' => $request->message,
            'offer_id' => $request->offer_id,

        ]);

        if ($request->file) {
            $file = $request->file('file');

            $mediaName = bin2hex(random_bytes(10)) . date("YmdHis") . '.' . $file->getClientOriginalExtension();
            $file->move(
                base_path() . '/public/uploads/chats/', $mediaName
            );
            $message->update(['file' => '/uploads/chats/' . $mediaName]);
        }

        //notification
        $title = 'تدلل';
        $content = 'رسالة جديدة';
        $fcm_message = [
            'message_id' => (int)$message->id,
            'from_user_id' => (int)$user->id,
            "is_seen" => (int)$message->is_seen,
            "message" => $message->message ?: null,
            "offer_id" => (int)$message->offer_id,
            'since' => $message->created_at->diffForHumans(),
            'created_at' => $message->created_at,
            'file' => \Helpers::base_url() . $message->file ?: null,
            'type' => 'message',
            "sender" => [
                'id' => $user->id,
                'name' => $message->sender->name ?: "",
                'photo' => \Helpers::base_url() . $message->sender->photo ?: "",
                'tokens' => $message->sender->tokens,
            ]
        ];

        Notification::send($to_user, new NewMessageNotification($message));
        \Helpers::fcm_notification($to_user->device_token, $content, $title, $fcm_message);

        return response()->json([], 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cost' => 'required',
            'offer_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $offer = Offer::find($request->offer_id);

        $invoice = Invoice::create([
            'cost' => $request->cost,
            'offer_id' => $request->offer_id,

        ]);

        if ($request->image) {
            $file = $request->file('image');

            $mediaName = bin2hex(random_bytes(10)) . date("YmdHis") . '.' . $file->getClientOriginalExtension();
            $file->move(
                base_path() . '/public/uploads/invoices/', $mediaName
            );

            $invoice->update(['image' => '/uploads/invoices/' . $mediaName]);
            //send message
            $message = Message::create([
                'from_user_id' => $user->id,
                'to_user_id' => $offer->order->user_id,
                'offer_id' => $offer->id,
                'file' => '/uploads/invoices/' . $mediaName,

            ]);
        }


        //notification
        $title = 'تدلل';
        $content = 'فاتورة جديدة';
        $message = [
            'type' => 'invoice',
            "offer_id" => (int)$invoice->offer_id,
        ];
        Notification::send($invoice->offer->order->user, new NewInvoiceNotification($invoice));
        \Helpers::fcm_notification($invoice->offer->order->user->device_token, $content, $title, $message);

        return response()->json([], 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePaymentType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_type' => 'required',
            'offer_id' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $offer = Offer::find($request->offer_id);
        $offer->order->update(['payment_type' => $request->payment_type]);
        return response()->json([], 204);
    }

    /**
     * @param $offer_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInvoice($offer_id)
    {
        $tax = Setting::first()->tax;

        $offer = Offer::find($offer_id);
        $invoice = collect();
        if ($offer->order->coupon_id) {

            $coupon = Coupon::find($offer->order->coupon_id);
            $invoice['discount'] = ($offer->delivary_cost * $coupon->discount) / 100;

        } else {
            $invoice['discount'] = 0;
        }

        $invoice['payment_type'] = $offer->order->payment_type;
        $invoice['cost'] = $offer->invoice->cost;
        $invoice['delivary_cost'] = $offer->delivary_cost;
        $invoice['tax'] = ($offer->invoice->cost * $tax) / 100;
//        $invoice['total'] = $invoice['cost'] + $invoice['delivary_cost'] + $invoice['tax'] - $invoice['discount'];
        $invoice['total'] = $invoice['cost'] + $invoice['delivary_cost'] - $invoice['discount'];

        return response()->json($invoice, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function chatDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'offer_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $to_user = User::where('id', $request->user_id)->first();

        $data = Message::select(['id', 'message', 'file', 'to_user_id', 'from_user_id', 'offer_id', 'is_seen', 'created_at'])->where('offer_id', $request->offer_id)
            ->where(function ($query) use ($user, $to_user) {
                $query->where([['from_user_id', $user->id], ['to_user_id', $to_user->id]]);
                $query->orWhere([['from_user_id', $to_user->id], ['to_user_id', $user->id]]);
            })->orderBy('created_at', 'asc')->with(['sender' => function ($q) {
                $q->select(['id', 'name', 'tokens', 'photo'])->get();
            }]);
        $messages = $data->get();
        $messages->transform(function ($i) {
            $i->since = $i->created_at->diffForHumans();
            if ($i->file) {
                $i->file = \Helpers::base_url() . '/' . $i->file;
            }
            return $i;
        });

        //read messages
        $data->update(['is_seen' => 1]);
        $offer = Offer::find($request->offer_id);
        $invoice = Invoice::where('offer_id', $request->offer_id)->first();
        $order = \App\Models\Order::where('id', $offer->order_id)->select(['id', 'order_number', 'type', 'status', 'place_id'])->first();

        if ($order->type == 1) {
            $store_name = $order->store ? (\Helpers::getLang() == 'ar' ? $order->store->name_ar : $order->store->name_en) : \Helpers::getPlaceCoordination($order->place_id)['name'];
            $store_icon = $order->store ? \Helpers::base_url() . '/' . $order->store->logo : \Helpers::getPlaceCoordination($order->place_id)['icon'];
        } else {
            $store_name = "";
            $store_icon = "";
        }

        if ($order->status == "InProgress")
            $is_active = true;
        else
            $is_active = false;

        $order['status'] = \Helpers::getLang() == 'en' ? (string)$order->status : \Helpers::getOrderStatus($order->status);
        $order['store_name'] = $store_name;
        $order['store_image'] = $store_icon;
        $order['has_invoice'] = $invoice ? true : false;
        $order['is_active'] = $is_active;
        $captain = User::where('id', $offer->captain_id)->select(['id', 'name', 'phone', 'photo', 'latitude', 'langitude'])->first();
        $client = User::where('id', $offer->order->user_id)->select(['id', 'name', 'phone', 'photo', 'latitude', 'langitude'])->first();
        $captain['photo'] = $captain->photo ? \Helpers::base_url() . $captain->photo : '';
        $captain['latitude'] = (double)$captain->latitude;
        $captain['langitude'] = (double)$captain->langitude;
        $captain['delivary_cost'] = $offer->delivary_cost;
        $client['latitude'] = (double)$client->latitude;
        $client['langitude'] = (double)$client->langitude;
        $client['rate_degree'] = $client->user_rates->avg('degree') ?: 0;
        $client['photo'] = \Helpers::base_url() . $client->photo;
        unset($order['store']);
        unset($client['user_rates']);
        return response()->json([
            'messages' => $messages,
            'order' => $order,
            'captain' => $captain,
            'client' => $client
        ], 200);
    }
}
