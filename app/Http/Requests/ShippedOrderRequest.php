<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ShippedOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "details" => "required",
            "shipping_method" => "required",
            "delivary_point_latitude" => "required",
            "delivary_point_langitude" => "required",
            "delivary_address" => "required",

            "order_point_latitude" => "required",
            "order_point_langitude" => "required",
            "order_address" => "required",
        ];
    }
    protected function failedValidation (Validator $validator)
    {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()->first()], 400));
    }
    public function messages ()
    {
        return [
//            "store_id.required" =>'store_id required',
//            "items.required" => 'items required',
            "details.required" => 'order details required',
            "shipping_method.required" => 'shipping method required',
            "delivary_point_latitude.required" => 'delivary point latitude required',
            "delivary_point_langitude.required" => 'delivary point langitude required',
            "delivary_address.required" => 'delivary address required',
            "order_point_latitude.required" => 'order point latitude required',
            "order_point_langitude.required" => 'order point langitude required',
            "order_address.required" => 'order address required',
        ];
    }
}
