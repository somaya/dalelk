<?php

namespace App\Http\Requests;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            "store_id" => "required",
//            "items" => "required",
            "details" => "required",
            "payment_type" => "required",
//            "delivary_point_latitude" => "required",
//            "delivary_point_langitude" => "required",
            "delivary_time" => "required",
//            "delivary_address" => "required",
        ];
    }
    protected function failedValidation (Validator $validator)
    {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()->first()], 400));
    }
    public function messages ()
    {
        return [
            "store_id.required" =>'store_id required',
//            "items.required" => 'items required',
            "details.required" => 'order details required',
            "payment_type.required" => 'payment type required',
//            "delivary_point_latitude.required" => 'delivary point latitude required',
//            "delivary_point_langitude.required" => 'delivary point langitude required',
//            "delivary_address.required" => 'delivary address required',
            "delivary_time.required" => 'delivary time required',
        ];
    }
}
