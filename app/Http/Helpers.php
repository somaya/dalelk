<?php

header("Content-Type: text/html; charset=utf-8");

use App\Models\Order;
use App\User;
use FCM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class Helpers
{

    public static function base_url()
    {
        return URL::to('/');
    }


    public static function the_image($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/700x300.png';
        }
    }

    public static function the_image_sm($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/70x70.png';
        }
    }

    public static function failFindId()
    {
        if (request()->header('Accept-Language') == 'ar') {
            $message = 'لا يوجد نتائج ';
        } else {
            $message = 'No results for this id';
        }
        return $message;
    }


    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param $unit
     * @return float|int
     */
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }


    /**
     * @return string
     */
    public static function generateRandomString()
    {
        return Str::random(255);

    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function CheckAuthorizedRequest()
    {
        return \request()->header('Access-Token');
    }

    /**
     * @return mixed
     */
    public static function getLoggedUser()
    {
        $user = User:: where('tokens', Helpers::CheckAuthorizedRequest())->first();
        if ($user)
            return $user;
        else
            return 'No results';
    }

    /**
     * @param $user
     * @return bool
     */
    public static function updateFCMToken($user)
    {
        $fcm_token = \request()->header('fcm-token');
        $user->update([
            'device_token' => $fcm_token ? $fcm_token : null
        ]);
        return true;
    }

    /**
     * @param $status
     * @return string
     */
    public static function getOrderStatus($status)
    {
        if ($status == 'Pending')
            $status = 'قيد الانتظار';
        elseif ($status == 'InProgress')
            $status = 'نشط';
        elseif ($status == 'Completed')
            $status = 'منتهي';
        elseif ($status == 'Canceled')
            $status = 'ملغي';

        return $status;
    }

    public static function directDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $earthRadius = 6371000;
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
        $angle = atan2(sqrt($a), $b);
        $in_km = ($angle * $earthRadius) / 1000;
        return round($in_km, 2);
    }


    /**
     * @param $token
     * @param $message
     * @return bool
     */
    public static function fcm_notification($token, $content, $title, $message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'notification' => $message,
            'sound' => true,
            'title' => $title,
            'body' => $content,
            'priority' => 'high',
        ];

        $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAAJw4d98U:APA91bHR3oI3WCeQ8V1Vzx4HTC7_pgH8kUcQIniyTgXD3otlpjKR8b00IG1r-4rrPmhCmwNWc6Uit7C10dgiJ9k_ge_znEpy4Rd3Ql3PaPQfCPsJVrdckqbcWI1Gg50rhhZRciFcZvju',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return true;
    }


    /**
     * @param $order_id
     * @return mixed
     */
    public static function getNearestDeliveries($order_id)
    {
        $order = Order::find($order_id);
        $distance = \App\Models\Setting::find(1);
        if ($order->place_id == null) {
            $deliveries = DB::table("users")
                ->where('type', 2)
                ->where('active', 1)
                ->select("users.id"
                    , DB::raw("6371 * acos(cos(radians(" . $order->store->latitude . "))
        * cos(radians(users.latitude))
        * cos(radians(users.langitude) - radians(" . $order->store->langitude . "))
        + sin(radians(" . $order->store->latitude . "))
        * sin(radians(users.latitude))) AS distance"))
                ->having('distance', '<', $distance->delivery_distance)
                ->groupBy('id')
                ->get();
        } else {
            $place_location = self::getPlaceCoordination($order->place_id);
            $deliveries = DB::table("users")
                ->where('type', 2)
                ->where('active', 1)
                ->select("users.id"
                    , DB::raw("6371 * acos(cos(radians(" . $place_location['lat'] . "))
        * cos(radians(users.latitude))
        * cos(radians(users.langitude) - radians(" . $place_location['lng'] . "))
        + sin(radians(" . $place_location['lat'] . "))
        * sin(radians(users.latitude))) AS distance"))
                ->having('distance', '<', $distance->delivery_distance)
                ->groupBy('id')
                ->get();
        }
        return $deliveries;
    }


    public static function getNearestDeliveriesForSpecialOrders($order_id)
    {
        $order = Order::find($order_id);
        $distance = \App\Models\Setting::find(1);
        $deliveries = DB::table("users")
            ->where('type', 2)
            ->where('active', 1)
            ->select("users.id"
                , DB::raw("6371 * acos(cos(radians(" . $order->order_point_latitude . "))
        * cos(radians(users.latitude))
        * cos(radians(users.langitude) - radians(" . $order->order_point_langitude . "))
        + sin(radians(" . $order->order_point_latitude . "))
        * sin(radians(users.latitude))) AS distance"))
            ->having('distance', '<', $distance->delivery_distance)
            ->groupBy('id')
            ->get();
        return $deliveries;
    }

    public static function getLang()
    {
        $lang = \request()->header('Accept-Language');
        return $lang ?: 'ar';
    }

    /**
     * @param $place_id
     * @return array
     */
    public static function getPlaceCoordination($place_id)
    {
        $googleKey = 'AIzaSyA4G9HchJv7Dy4Z2JCeHJaYvzX1cfbg0-M';
        $url = "https://maps.googleapis.com/maps/api/place/details/json?place_id=" . $place_id . "&fields=address_component,adr_address,business_status,formatted_address,geometry,icon,name,photo,place_id,plus_code,type,url,utc_offset,formatted_phone_number,opening_hours,rating,review,user_ratings_total&key=" . $googleKey;
        $json = file_get_contents($url);
        $data = json_decode($json);

        $status = $data->status;
        if ($status == "OK") {
            $result = $data->result;
            $categories = ['cafe', 'restaurant', 'supermarket', 'deserts'];
            $types = $result->types;
            if (isset($types[0])) {
                if (in_array($types[0], $categories)) {
                    $icon = url('img/icons/' . $types[0] . '.png');
                } else {
                    $icon = url('img/icons/store.png');
                }
            } else {
                $icon = url('img/icons/store.png');
            }
            $s_photos = [];
            $place_photos = [];
            if (isset($result->photos)) {
                foreach ($result->photos as $pic) {
                    $s_photos[] = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $pic->width . '&photoreference=' . $pic->photo_reference . '&key=' . $googleKey;
                }
                foreach ($s_photos as $key => $s_photo) {
                    $place_photos[$key] = $s_photo;
                }
            }

            $my_address = '';
            foreach ($result->address_components as $adr) {
                if ($adr->types[0] == 'postal_code')
                    continue;
                $my_address .= $adr->long_name . ' ';
            }

            $place = [
                'name' => $result->name,
                'lat' => doubleval($result->geometry->location->lat),
                'lng' => doubleval($result->geometry->location->lng),
                'address' => $my_address,
                'icon' => $icon,
                'place_images' => $place_photos,
            ];
            return $place;
        }
    }


//    /**
//     * @param $messageContent
//     * @param $mobileNumber
//     * @return false|string
//     */
//    public static function send_sms($messageContent, $mobileNumber)
//    {
//        $user = 'dalilk-app';
//        $password = '565656';
//        $sendername = 'Dalilk';
//        $text = urlencode($messageContent);
//        $to = $mobileNumber;
//        $url = "http://www.4jawaly.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=full";
//        $ret = file_get_contents($url);
//        return $ret;
//    }
}
