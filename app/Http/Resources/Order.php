<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = \Helpers::getLang();

        $data = [
            "id" => (int)$this->id,
            "type" => (int)$this->type,
            "order_number" => (integer)$this->order_number,
            "details" => (string)$this->details,
            "delivary_time" => (string)$this->delivary_time,
            "status" => $lang == 'en' ? (string)$this->status : \Helpers::getOrderStatus($this->status),
            "discount" => $this->coupon ? $this->coupon->discount : 0,
            "user_id" => $this->user_id,
        ];

        if ($this->place_id != null && $this->type == 1) {
            $place = \Helpers::getPlaceCoordination($this->place_id);
            $data["store_name"] = $place['name'];
            $data["store_images"] = $place['place_images'] ? $place['place_images'][0] : "";
            $data["store_icon"] = $place['icon'];
        } elseif ($this->place_id == null && $this->type == 1) {
            if ($this->store) {
                $data["store_name"] = \Helpers::getLang() == 'ar' ? $this->store->name_ar : $this->store->name_en;
                $data["store_image"] = $this->store->logo ? \Helpers::base_url() . '/' . $this->store->logo : '';
            } else {
                $data["store_name"] = "";
                $data["store_images"] = "";
            }
        } else {
            $data["store_name"] = "";
            $data["store_images"] = "";
        }

        $offer = \App\Models\Offer::where('order_id', $this->id)->where('status', 'Accepted')->first();

        if ($offer) {
            $data["offer_id"] = $offer->id;
            $data["captain_id"] = $offer->captain_id;
        } else {
            $data["offer_id"] = 0;
            $data["captain_id"] = 0;
        }
        return $data;
    }
}
