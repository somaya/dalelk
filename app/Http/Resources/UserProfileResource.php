<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class  UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => (int)$this->id,
            "type" => (int)$this->type,
            "name" => (string)$this->name,
            "email" => (string)$this->email,
            "image" => $this->photo ? \Helpers::base_url() . '/' . $this->photo : '',
            "phone" => (string)$this->phone,
            "latitude" => (string)$this->latitude,
            "langitude" => (string)$this->langitude,
            "birtyear" => (string)$this->birthyear,
            "birthdate" => (string)$this->birthdate,
            "national_id" => (string)$this->national_id,
            "rate_degree" =>$this->user_rates->isNotEmpty()?($this->user_rates->sum('degree')/$this->user_rates->count()):0,

        ];

    }
}
