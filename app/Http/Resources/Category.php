<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => App()->getLocale() == 'ar' ? $this->name_ar : $this->name_en,
            'type' => $this->name_en,
            'photo' => \Helpers::base_url() . '/' . $this->photo,
        ];
    }
}
