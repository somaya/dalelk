<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Adv extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'photo' =>\Helpers::base_url().'/' . $this->photo,
            'title' =>App()->getLocale()=='ar'?$this->title_ar:$this->title_en,
        ];
    }
}
