<?php

namespace App\Http\Resources;

use App\Models\SavedAdress;
use App\Models\User_rates;
use Illuminate\Http\Resources\Json\JsonResource;

class Offer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $rates = User_rates::where('captain_id', $this->captain_id);
        if ($this->order->address_id) {
            $address = SavedAdress::find($this->order->address_id);
            $delivery_point_latitude = (double)$address->latitude;
            $delivery_point_longitude = (double)$address->langitude;
        } else {
            $delivery_point_latitude = (double)$this->order->delivary_point_latitude;
            $delivery_point_longitude = (double)$this->order->delivary_point_langitude;
        }

        return [
            "id" => (int)$this->id,
            "order_number" => (integer)$this->order->order_number,
            "delivary_time" => (string)$this->order->delivary_time,
            "delivary_cost" => (float)$this->delivary_cost,
            "status" => (string)$this->status,
            "captain_name" => $this->captain->name,
            "rate_count" => $rates->count() ?: 0,
            "rate_degree" => (float)$rates->avg('degree') ?: 0,
            "captain_image" => $this->captain->photo ? \Helpers::base_url() . '/' . $this->captain->photo : '',
            "delivery_point_latitude" => $delivery_point_latitude,
            "delivery_point_longitude" => $delivery_point_longitude,
            "captain_latitude" => (double)$this->captain->latitude ?: 0,
            "captain_longitude" => (double)$this->captain->langitude ?: 0,
            "delivery_time" => (string)$this->order->delivary_time,
            "delivery_cost" => (float)$this->delivary_cost,
        ];
    }
}
