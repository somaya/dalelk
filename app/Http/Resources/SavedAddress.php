<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SavedAddress extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "name" => (string)$this->name,
            "latitude" => (string)$this->latitude,
            "langitude" => (string)$this->langitude,
            "address" => (string)$this->address,
        ];
    }
}
