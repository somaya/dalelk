<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PendingOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = \Helpers::getLang();
        $data = [
            "id" => (int)$this->id,
            "order_number" => (integer)$this->order_number,
            "delivery_point_latitude" => (double)$this->delivary_point_latitude,
            "delivery_point_longitude" => (double)$this->delivary_point_langitude,
            "delivery_address" => (string)$this->delivary_address,
            "status" => $lang == 'en' ? (string)$this->status : \Helpers::getOrderStatus($this->status),
            "type" => (int)$this->type,
        ];

        if ($this->type == 2 || $this->type == 3) {
            $data["order_point_latitude"] = (double)$this->delivary_point_latitude;
            $data["order_point_longitude"] = (double)$this->delivary_point_langitude;
            $data["order_address"] = (string)$this->delivary_address;
        }

        if ($this->place_id != null && $this->type == 1) {
            $place = \Helpers::getPlaceCoordination($this->place_id);
            $data["store_name"] = $place['name'];
            $data["store_images"] = $place['place_images'] ? $place['place_images'][0] : "";
            $data["store_icon"] = $place['icon'];
            $data["store_address"] = $place['address'];
            $data["store_longitude"] = $place['lng'];
            $data["store_latitude"] = $place['lat'];
        } elseif ($this->place_id == null && $this->type == 1) {
            if ($this->store) {
                $data["store_name"] = \Helpers::getLang() == 'ar' ? $this->store->name_ar : $this->store->name_en;
                $data["store_image"] = $this->store->logo ? \Helpers::base_url() . '/' . $this->store->logo : '';
                $data["store_address"] = $this->store->address;
                $data["store_longitude"] = $this->store->langitude;
                $data["store_latitude"] = $this->store->latitude;
            } else {
                $data["store_name"] = "";
                $data["store_images"] = "";
                $data["store_address"] = "";
                $data["store_longitude"] = "";
                $data["store_latitude"] = "";
            }
        } else {
            $data["store_name"] = "";
            $data["store_images"] = "";
            $data["store_address"] = "";
            $data["store_longitude"] = "";
            $data["store_latitude"] = "";
        }
        return $data;
    }
}
