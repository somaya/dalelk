<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Choice extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' =>$this->product_id,
            'choice_id' =>$this->choice_id,
            'product' =>$this->product,

        ];
    }
}
