<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = (request()->header('lang')) ?? 'ar';
        $distance = \Helpers::directDistance($request->lat, $request->long, $this->latitude, $this->langitude);
        $distance = ($lang == 'ar') ? $distance . " كم" : $distance . " KM";

        return [
            "id" => (int)$this->id,
            "name" => \Helpers::getLang() == 'ar' ? $this->name_ar : $this->name_en,
            "icon" => $this->logo ?: "",
            "address" => $this->address ?: "",
            "lat" => doubleval($this->latitude),
            "lng" => doubleval($this->langitude),
            'distance' => $distance,
            'place_id' => "",
        ];
    }
}
