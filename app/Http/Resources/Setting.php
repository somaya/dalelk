<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Setting extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'logo' => \Helpers::base_url().'/' . $this->logo,
            'terms' => App()->getLocale()=='ar'?$this->terms_ar:$this->terms_en,
            'privacy' =>App()->getLocale()=='ar'?$this->privacy_ar:$this->privacy_en,
        ];
    }
}
