<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserRates extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "degree" => (integer)$this->degree,
            "comment" => (string)$this->comment,
            "since" => $this->created_at->format('Y-m-d H:i:s'),
            "captain_name" =>$this->captain->name,
            "captain_image" => $this->captain->photo?\Helpers::base_url().'/' . $this->captain->photo:'',
        ];
    }
}
