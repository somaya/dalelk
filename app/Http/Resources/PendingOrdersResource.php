<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PendingOrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = \Helpers::getLang();
        $data = [
            "id" => (int)$this->id,
            "type" => (int)$this->type,
            "order_number" => (integer)$this->order_number,
            "details" => (string)$this->details,
            "delivary_time" => (string)$this->delivary_time,
            "delivary_point_latitude" => (string)$this->delivary_point_latitude,
            "delivary_point_langitude" => (string)$this->delivary_point_langitude,
            "delivary_address" => (string)$this->delivary_address,
            "status" => $lang == 'en' ? (string)$this->status : \Helpers::getOrderStatus($this->status),
            "discount" => $this->coupon ? $this->coupon->discount : 0,
        ];

        if ($this->place_id != null && $this->type == 1) {
            $place = \Helpers::getPlaceCoordination($this->place_id);
            $data["store_name"] = $place['name'];
            $data["store_images"] = $place['place_images'] ? $place['place_images'][0] : "";
            $data["store_icon"] = $place['icon'];
            $data["store_address"] = $place['address'];
            $data["store_longitude"] =$place['longitude'];
            $data["store_latitude"] =$place['latitude'];
        } elseif ($this->place_id == null && $this->type == 1) {
            if ($this->store) {
                $data["store_name"] = \Helpers::getLang() == 'ar' ? $this->store->name_ar : $this->store->name_en;
                $data["store_image"] = $this->store->logo ? \Helpers::base_url() . '/' . $this->store->logo : '';
                $data["store_address"] = $this->store->address;
                $data["store_longitude"] = $this->store->langitude;
                $data["store_latitude"] = $this->store->latitude;
            } else {
                $data["store_name"] = "";
                $data["store_images"] = "";
                $data["store_address"] = "";
                $data["store_longitude"] = "";
                $data["store_latitude"] = "";
            }
        } else {
            $data["store_name"] = "";
            $data["store_images"] = "";
            $data["store_address"] = "";
            $data["store_longitude"] = "";
            $data["store_latitude"] = "";
        }
        return $data;
    }
}
