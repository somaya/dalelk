<?php

namespace App\Notifications;


use App\Models\Offer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class RefurseOfferDeliveryNotification extends Notification
{
    use Queueable;

    protected $offer;

    public function __construct(Offer $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function ToDatabase($notifiable)
    {
        return [
            'type' => 'reject_offer',
            'name' => 'رفض عرض سعر',
            'data' => 'رفض عرض سعر بخصوص الطلب' . ' ' . $this->offer->order->order_number,
            "offer_id" => (int)$this->offer->id,
            "order_id" => (int)$this->offer->order->id,
            "order_number" => (int)$this->offer->order->order_number,
            "captain_id" => (int)$this->offer->captain->id,
            "user_id" => (int)$this->offer->order->user_id,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
