<?php

namespace App\Notifications;


use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class NewShippedOrderDeliveryNotification extends Notification
{
    use Queueable;

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function ToDatabase($notifiable)
    {
        return [
            'order_id' => $this->order->id,
            'name' => 'طلب نقل امتعة وارد',
            'type' => 'shipped_order',
            'is_pending' => $this->order->status == "Pending" ? true : false,
            'data' => 'هناك طلب نقل امتعة وارد برقم' . ' ' . $this->order->order_number,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
