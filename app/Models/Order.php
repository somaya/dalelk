<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'store_orders';
    protected $guarded = [];

    public $timestamps = true;

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function images()
    {
        return $this->hasMany(order_image::class, 'order_id');
    }

}
