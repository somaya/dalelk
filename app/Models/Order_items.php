<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_items extends Model
{
    protected $table = 'store_order_item';
    protected $guarded = [];

    public $timestamps = true;
}
