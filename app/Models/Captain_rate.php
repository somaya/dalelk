<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Captain_rate extends Model
{
    protected $table = 'captain_rates';
    protected $guarded = [];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
