<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = 'offers';
    protected $guarded = [];

    public $timestamps = true;

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    public function captain()
    {
        return $this->belongsTo(User::class, 'captain_id');
    }
    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'offer_id');
    }
}
