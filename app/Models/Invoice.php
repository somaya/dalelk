<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $guarded = [];

    public $timestamps = true;

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }
}
