<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    protected $guarded = [];

    public $timestamps = true;

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    }
    public function sender()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }
}
