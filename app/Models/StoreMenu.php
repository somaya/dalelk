<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreMenu extends Model
{
    protected $table = 'menues';
    protected $guarded = [];

    public $timestamps = true;

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
