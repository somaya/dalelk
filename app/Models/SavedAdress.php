<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SavedAdress extends Model
{
    protected $table = 'saved_address';
    protected $guarded = [];

    public $timestamps = true;
}
