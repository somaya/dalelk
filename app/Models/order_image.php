<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class order_image extends Model
{
    protected $table = 'store_order_images';
    protected $guarded = [];

    public $timestamps = true;
}
