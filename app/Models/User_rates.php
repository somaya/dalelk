<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class User_rates extends Model
{
    protected $table = 'user_rates';
    protected $guarded = [];

    public $timestamps = true;

    public function captain()
    {
        return $this->belongsTo(User::class, 'captain_id');
    }
}
