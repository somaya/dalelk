<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';
    protected $guarded = [];

    public $timestamps = true;
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
