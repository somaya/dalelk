<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';
    protected $guarded = [];

    public $timestamps = true;

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
