<?php

use App\Contact;
use App\Coupons;
use App\History;
use App\Html;
use App\orderWithdrawReasons;
use App\Payments;
use App\Permission;
use App\Role;
use App\Setting;
use App\SmsEmailNotification;
use App\User;
use App\userBlocks;
use App\userMeta;
use App\usersCoupons;
use Illuminate\Support\Facades\Route;
use Jenssegers\Date\Date;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Twilio\Jwt\ClientToken;
use Twilio\Rest\Client;

function getUserCouponDiscount($user_id = false, $price = 0)
{
    $have_coupon = false;
    $discount = 0.0;
    if ($usercoupon = usersCoupons::where(['user_id' => $user_id, 'used' => 'false'])->where('end_at', '>=', date('Y-m-d'))->first()) {
        $coupon_id = $usercoupon->coupon_id;
        $have_coupon = true;
        if ($coupon = Coupons::find($coupon_id)) {
            if ($coupon->type == 'percentage') {
                $discount = floatval($price) * (floatval($coupon->value) / 100);
            } else {
                if (floatval($coupon->value) > floatval($price)) {
                    $discount = floatval($price);
                } else {
                    $discount = floatval($coupon->value);
                }
            }
        }
        $usercoupon->used = 'true';
        $usercoupon->save();
    }

    return ['discount' => round($discount, 2), 'have_coupon' => $have_coupon];
}

function nationalities()
{
    return [
        'Saudi' => 'سعودي',
    ];
}

function smtp($type)
{
    $smtp = SmsEmailNotification::where('type', '=', 'smtp')->first();
    return $smtp->$type;
}

function savePayment($user_id = 0, $second_user_id = 0, $amount = 0, $type = '', $operation = '', $status = 'inprogress', $country_id = 1, $order_id = 0, $credit_card = null)
{
    $payment = new Payments();
    $payment->user_id = $user_id;
    $payment->second_user_id = $second_user_id;
    $payment->order_id = $order_id > 0 ? $order_id : null;
    $payment->amount = $amount;
    $payment->type = $type;
    $payment->credit_card = $credit_card;
    $payment->status = $status;
    $payment->operation = $operation;
    $payment->country_id = $country_id;
    $payment->save();
    return $payment->id;
}


function checkEnoughBalanceAppPercentage($price = 0, $balance = 0)
{
    $app_percentage = floatval(floatval($price) * (floatval((setting('site_percentage'))) / 100));
    $msg = '';
    if (floatval($app_percentage) > floatval($balance)) {
        $debt = floatval($app_percentage - floatval($balance));
        if (setting('allow_debt') == 'true') {
            if ($debt > 25) {
                $msg = trans('order.not_enough_balance');
            }
        } else {
            $msg = trans('order.not_enough_balance');
        }
    }
    return $msg;
}

function is_success($code)
{

    $arr = [
        '000.000.000',
        '000.000.100',
        '000.100.110',
        '000.100.111',
        '000.100.112',
        '000.300.000',
        '000.300.100',
        '000.300.101',
        '000.300.102',
        '000.600.000',
        '000.200.100'
    ];

    return in_array($code, $arr) ? true : false;
}

function checkUserBlock($user_id = '')
{
    $msg = '';
    if ($block = userBlocks::where('user_id', '=', $user_id)->orderBy('created_at', 'DESC')->first()) {
        $to_time = strtotime($block->to_time);
        $from_time = strtotime(date('Y-m-d H:i:s'));
        $stillhours = round(($to_time - $from_time) / 3600, 2);
        if ($stillhours > 0) {
            $to_time = Date::parse($to_time)->format('l j F h:i ');
            $to_time .= trans('order.' . date('a', strtotime($block->to_time)));
            $msg = trans('user.haveBlock', ['date' => $to_time]);
        } else {
            $block->delete();
        }
    }
    return $msg;
}

function checkWithdarwBlock($user_id = '')
{
    $msg = '';
    if ($num_today_withdraw = orderWithdrawReasons::where(['user_id' => $user_id, 'date' => date('Y-m-d')])->count()) {
        if ($num_today_withdraw == 4) {
            $lastWithdraw = orderWithdrawReasons::where(['user_id' => $user_id, 'date' => date('Y-m-d')])->orderBy('created_at', 'DESC')->first();
            $hourdiff = round((strtotime('now') - strtotime($lastWithdraw->created_at)) / 3600, 1);
            if (floatval($hourdiff) < floatval(4)) {
                $msg = trans('order.blockmsg', ['name' => '']);
            }
        }
    }
    return $msg;
}

function checkApplyBidPrice($price = 0, $lang = 'ar', $order)
{
    $msg = '';
    if (floatval($price) < floatval(setting('min_order_price'))) {
        $msg = trans('order.min_order_price') . ' ' . setting('min_order_price') . ' ' . setting('site_currency_' . $lang);
    } else {
        if ((floatval($price) < floatval($order->min_expected_price))) {
            $msg = trans('order.min_order_price') . ' ' . floatval($order->min_expected_price) . ' ' . setting('site_currency_' . $lang);
        }
    }
    return $msg;
}

function currentCountry()
{
    $ip = '';
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR']; // This will contain the ip of the request
    }
    $data = array('iso' => 'SA',          // EG
        'name' => 'saudi arabia',//"Egypt"
        'currency' => 'SAR',         //"EGP"
        'symbol' => 'SR',          // "£"
        'ratio' => '3.750',       //to USD  "17.3873"
        'time_zone' => 'Asia/Riyadh'
    );
    $url = "http://www.geoplugin.net/json.gp?ip=" . $ip;
    // if(is_readable($url)){
    $geoplugin = @file_get_contents($url, true);
    if ($geoplugin === FALSE) {
        return $data;
    } else {
        $dataArray = json_decode($geoplugin);
        if ($dataArray) {
            $data = array('iso' => $dataArray->geoplugin_countryCode,    // EG
                'name' => $dataArray->geoplugin_countryName,    //"Egypt"
                'currency' => $dataArray->geoplugin_currencyCode,   //"EGP"
                'symbol' => $dataArray->geoplugin_currencySymbol, // "£"
                'ratio' => $dataArray->geoplugin_currencyConverter, //to USD  "17.3873"
                'time_zone' => $dataArray->geoplugin_timezone
            );
        }
    }
    // }
    return $data;
}

function directDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $earthRadius = 6371000;
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
    $angle = atan2(sqrt($a), $b);
    $in_km = ($angle * $earthRadius) / 1000;
    return round($in_km, 2);
}

function GetDrivingDistance($lat1 = '', $long1 = '', $lat2 = '', $long2 = '', $lang = 'ar')
{
    $google_key = setting('google_places_key');
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=" . $lang . "&key=" . $google_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($result, true);
    if ($response['rows']) {
        if ($response['rows'][0]['elements'][0]['status'] == 'ZERO_RESULTS' || ($response['rows'][0]['elements'][0]['status'] == 'NOT_FOUND')) {
            $distance = directDistance($lat1, $long1, $lat2, $long2);
            $distance = $distance * 1000; // in meter
        } else {
            $distance = $response['rows'][0]['elements'][0]['distance']['value'];     // in Meter
        }
    } else {
        $distance = directDistance($lat1, $long1, $lat2, $long2);
        $distance = $distance * 1000;  // in Meter
    }
    $in_kms = ($distance / 1000); //in kms
    return round($in_kms, 2);
}

function checkBlock($user_id = false)
{
    if ($user_id != false) {
        if ($block = userBlocks::where('user_id', '=', $user_id)->first()) {
            $to_time = strtotime($block->to_time);
            $from_time = strtotime(date('Y-m-d H:i:s'));
            $stillhours = intval(round(($to_time - $from_time) / 3600, 2));
            if ($stillhours > 0) {
                $date = Date::parse($block->to_time)->format('H:i');
                return "حظر طلبات (" . $date . ")";
            } else {
                $block->delete();
                return 'true';
            }
        }
        return 'true';
    }
    return 'true';
}

function setting($key = false)
{
    if ($key != false) {
        if ($s = Setting::where('set_key', '=', $key)->first()) {
            return $s->set_value;
        }
        return false;
    }
    return false;
}

function successReturn($data = array(), $msg = '')
{
    return ['value' => '1', 'key' => 'success', 'data' => $data, 'msg' => $msg, 'code' => 200];
}

function successReturnMsg($msg = '')
{
    return ['value' => '1', 'key' => 'success', 'msg' => $msg, 'code' => 200];
}

function failReturn($msg = '')
{
    return ['value' => '0', 'key' => 'fail', 'msg' => $msg, 'code' => 401];
}

function failReturnData($data = [])
{
    return ['value' => '0', 'key' => 'fail', 'data' => $data, 'code' => 401];
}

//calculate if pray time near
function praytime($from_lat = '', $from_long = '', $lang = 'ar')
{
    $praytimes = [];
    $msg = '';
    $url = "http://api.aladhan.com/v1/calendar?latitude=" . $from_lat . "&longitude=" . $from_long . "&method=4&month=" . date("m") . "&year=" . date("Y");
    $jsonresult = @file_get_contents($url, true);
    if ($jsonresult === FALSE) {
        return '';
    } else {
        if ($results = json_decode($jsonresult)) {
            if (isset($results->data)) {
                $currentday = intval(date('d')) - 1;
                $praytimes['Fajr'] = $results->data[$currentday]->timings->Fajr;
                $praytimes['Dhuhr'] = $results->data[$currentday]->timings->Dhuhr;
                $praytimes['Asr'] = $results->data[$currentday]->timings->Asr;
                $praytimes['Maghrib'] = $results->data[$currentday]->timings->Maghrib;
                $praytimes['Isha'] = $results->data[$currentday]->timings->Isha;
                foreach ($praytimes as $key => $value) {
                    $praytime = substr($value, 0, 5);
                    $to_time = strtotime(date("Y-m-d") . " " . $praytime);
                    $from_time = strtotime(date('Y-m-d H:i'));
                    $minutes = intval(round(($to_time - $from_time) / 60, 2));
                    if (($minutes <= 30) && ($minutes >= 0)) {
                        $msg = setting('pray_msg_' . $lang);
                    }
                }
            }
        }
    }
    return $msg;
}

function notify($user_id = false, $notifier_id = false, $message = '', $data = '', $status = '', $key = '')
{
    if (($user_id != false) && ($message != '') && ($data != '')) {
        $notification = new Notifications();
        $notification->user_id = $user_id;
        $notification->notifier_id = $notifier_id;
        $notification->message = $message;
        $notification->data = $data;
        $notification->order_status = $status;
        $notification->key = $key;
        $notification->save();
        return true;
    }
}

function sendNotification($devices = [], $message = '', $title = '', $data = [], $neworder = '')
{
    $iosTokens = [];
    $androidTokens = [];
    if (count($devices) > 0) {
        foreach ($devices as $device) {
            if ($neworder == 'newOrder') {
                if ($device->near_orders_notify == 'true') {
                    if ($device->device_type == 'ios') {
                        $iosTokens[] = $device->device_id;
                    } else {
                        $androidTokens[] = $device->device_id;
                    }
                }
            } elseif ($neworder == 'newOrderwithPlace') {
                if ($device->orders_notify == 'true') {
                    if ($device->device_type == 'ios') {
                        $iosTokens[] = $device->device_id;
                    } else {
                        $androidTokens[] = $device->device_id;
                    }
                }
            } else {
                if ($device->device_type == 'ios') {
                    $iosTokens[] = $device->device_id;
                } else {
                    $androidTokens[] = $device->device_id;
                }
            }
        }
    }
    if (count($iosTokens) > 0) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)
            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $databuild = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($iosTokens, $option, $notification, $databuild);
    }
    if (count($androidTokens) > 0) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)
            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = Null;
        $databuild = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($androidTokens, $option, $notification, $databuild);
    }
    // $tokens = "dx0vu3gBtho:APA91bE-X-6yjUmZd-dbpchOt-qPC4sJXDfs_0sDuEXwfviluD5E3FcWCQv0NXRTs_De6Ja_2eNeizw9yz7IBa2UrIJBNyHK0FrmPQRArq2mzv355MWrH1DbWFhA1vRqS67gO3v_iVAu";//MYDATABASE::pluck('fcm_token')->toArray();
}

#role name
function Role()
{
    $role = Role::findOrFail(Auth::user()->role);
    if ($role) {
        return $role->role;
    } else {
        return 'عضو';
    }
}

#messages notification
function Notification()
{
    $messages = Contact::where('showOrNow', 0)->latest()->get();
    return $messages;
}

function newUserMetas()
{
    $usermetas = userMeta::where('seen', '=', 'false')->latest()->get();
    return $usermetas;
}

#upload image base64
function save_img($base64_img, $img_name, $path)
{
    $full_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $path;
    $image_data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64_img));
    $image_data;
    $source = imagecreatefromstring($image_data);
    $angle = 0;
    $rotate = imagerotate($source, $angle, 0); // if want to rotate the image
    $imageName = $img_name . '.png';
    $path_new = $full_path . '/' . $imageName;
    $imageSave = imagejpeg($rotate, $path_new, 100);
    if ($imageSave) {
        return true;
    } else {
        return false;
    }
}


#report
function History($user_id, $event)
{
    $report = new History;
    $user = User::findOrFail($user_id);
    if ($user->role > 0) {
        $report->user_id = $user->id;
        $report->event = 'قام ' . $user->name . ' ' . $event;
        $report->supervisor = 1;
        $report->save();
    } else {
        $report->user_id = $user->id;
        $report->event = 'قام ' . $user->name . ' ' . $event;
        $report->supervisor = 0;
        $report->save();
    }

}

#current route
function currentRoute()
{
    $routes = Route::getRoutes();
    foreach ($routes as $value) {
        if ($value->getName() === Route::currentRouteName()) {
            echo $value->getAction()['title'];
        }
    }
}

#email colors
function EmailColors()
{
    $html = Html::select('email_header_color', 'email_footer_color', 'email_font_color')->first();
    return $html;
}

function phoneValidate($number = '')
{
    if (substr($number, 0, 1) === '0') {
        $number = substr($number, 1);
    }
    if (substr($number, 0, 4) === '+966') {
        $number = substr($number, 4);
    }
    if (substr($number, 0, 4) === '0966') {
        $number = substr($number, 4);
    }
    if (substr($number, 0, 3) === '+20') {
        $number = substr($number, 3);
    }
    if (substr($number, 0, 3) === '020') {
        $number = substr($number, 3);
    }
    $phone = preg_replace('/\s+/', '', $number);
    return $phone;
}

function convert2english($string)
{
    $newNumbers = range(0, 9);
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    $string = str_replace($arabic, $newNumbers, $string);
    return $string;
}

function is_unique($key, $value)
{
    $user = User::where($key, $value)->first();
    if ($user) {
        return true;
    }
    return false;
}


function generate_code()
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $token = '';
    $length = 4;
    for ($i = 0; $i < $length; $i++) {
        $token .= $characters[rand(0, $charactersLength - 1)];
    }
    if ($user = User::where(['code' => $token])->first()) {
        return generate_code();
    }
    return $token;
}


function safasms($username, $sender, $password, $numbers, $msg)
{
    $text = urlencode($msg);
    $sender = urlencode($sender);
    $url = "http://www.safa-sms.com/api/sendsms.php?username=$username&password=$password&numbers=$numbers&message=$text&sender=$sender&unicode=E&return=full";
    $result = file_get_contents($url, true);
    if ($result === false) {
        return false;
    } else {

        return true;
    }
}

#send sms
function send_mobile_sms($numbers, $msg)
{

    $username = 'azhmny';
    $sender = 'azhmny';
    $password = '115599';

    safasms($username, $sender, $password, $numbers, $msg);

    return true;
}

function upload_img($base64_img, $path)
{
    $file = base64_decode($base64_img);
    $safeName = str_random(10) . '.' . 'png';
    file_put_contents($path . $safeName, $file);
    return $safeName;
}
