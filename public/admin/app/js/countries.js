
$(document).ready(function () {

    $('#city_id').on('change', function (e) {
        var city_id = e.target.value;
        if (city_id)
        {

            $.ajax({
                url: '/getStates?city_id=' + city_id,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#state_id').empty();

                        $('#state_id').append('<option value="">اختر الحي</option>');
                        $.each(data, function (i, m) {


                            $('#state_id').append('<option value="' + m.id + '">' + m.name_ar + '</option>');

                        });

                }


            });
        } else {
            $('#state_id').empty();
        }
    });

});






