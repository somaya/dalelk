<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'Api', 'middleware' => 'language'], function () {

    Route::get('/update_language/{lng}', 'HomeController@updateLanguage');
// lists
    Route::post('get_home', 'HomeController@getHome');
    Route::get('get_categories', 'HomeController@getCategories');
//    Route::get('get_branches', 'HomeController@getBranches');
    Route::get('get_settings', 'HomeController@getSettings');
    Route::post('contact_us', 'HomeController@contactUs');
    Route::get('get_stores', 'HomeController@getStores');
    Route::post('get_place_details', 'HomeController@getPlaceDetails');
    Route::post('get_stores_by_category', 'HomeController@getStoresByCategories');


    // user
    Route::post('register', 'UserController@register');
    Route::post('verify_code', 'UserController@verifyUser');
    Route::post('user_resend_code', 'UserController@resendUserActivationCode');
    Route::get('logout', 'UserController@logout');

    Route::get('get_profile', 'UserController@getProfile');
    Route::post('update_profile', 'UserController@updateProfile');
    Route::post('update_location', 'UserController@updateLocation');
    Route::post('update_profile_image', 'UserController@changeProfilePicture');
    Route::get('notifiable', 'UserController@notifiable');
    Route::post('update_user_token', 'UserController@updateUserFCMToken');
    //order

    Route::post('add_order', 'OrderController@addOrder');
    Route::post('add_special_order', 'OrderController@addSpecialOrder');
    Route::post('add_shipped_order', 'OrderController@addShippedOrder');
    Route::post('active_coupon', 'OrderController@activeCoupon');
    Route::post('save_address', 'OrderController@saveAddress');
    Route::get('get_saved_address', 'OrderController@getSavedAddress');
    Route::get('delete_saved_address/{id}', 'OrderController@deleteSavedAddress');
    Route::get('cancel_order/{id}', 'OrderController@cancelOrder');
    Route::get('get_orders', 'OrderController@getOrders');
    Route::get('delete_orders', 'OrderController@deleteOrders');
    Route::get('get_order_count/{id}', 'OrderController@getOrderCount');
    Route::get('get_portfolio', 'OrderController@getPortfolio');
    //offer
    Route::get('accept_offer/{id}', 'OffersController@acceptOffer');
    Route::get('reject_offer/{id}', 'OffersController@rejectOffer');
    Route::post('rate_captain', 'OffersController@rateCaptain');
    Route::post('rate_user', 'OffersController@rateUser');
    Route::post('send_offer', 'OffersController@sendOffer');
    Route::get('cancel_offer/{id}', 'OffersController@cancelOffer');
    Route::get('all_offers/{id}', 'OffersController@allOffers');
    Route::get('get_captains_rates/{id}', 'OffersController@getCaptainsRates');
    Route::get('get_users_rates/{id}', 'OffersController@getUsersRates');


    //products
    Route::get('get_products/{category}', 'HomeController@getProducts');
    Route::post('add_choice', 'ProductsController@addChoice');
    Route::post('maintenance_request', 'ProductsController@maintenanceRequest');
    Route::post('price_request', 'ProductsController@priceRequest');
    Route::post('contract_request', 'ProductsController@contractRequest');
    Route::get('get_my_choices', 'ProductsController@getMyChoices');
    Route::get('delete_choice/{id}', 'ProductsController@deleteChoice');

    // notifications
    Route::get('get_notifications', 'UserController@getNotifications');
    Route::get('/get_unread_notifications', 'UserController@unreadNotifications');
    Route::get('/delete_notifications', 'UserController@deleteNotification');
    Route::get('/delete_one_notification/{id}', 'UserController@deleteOneNotification');
    //messages
    Route::post('/send_message', 'MessagesController@sendMessage');
    Route::post('/chat_details', 'MessagesController@chatDetails');
    Route::post('/change_order_status', 'OrderController@changeOrderStatus');
    Route::post('/add_invoice', 'MessagesController@addInvoice');
    Route::post('/change_payment_type', 'MessagesController@changePaymentType');
    Route::get('/get_invoice/{id}', 'MessagesController@getInvoice');
    Route::any('search_stores','HomeController@searchStores');

    Route::any('get_pending_order/{order_id}','OrderController@getPendingOrder');
});
