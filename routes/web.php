<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();


Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/users', 'admin\UsersController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/deliveries', 'admin\DeliveriesController');
    Route::resource('/contacts', 'admin\ContactsController');
    Route::get('/contact/{id}/reply', 'admin\ContactsController@reply');
    Route::post('/contact/{id}/sendreply', 'admin\ContactsController@sendReply');
    Route::resource('/settings', 'admin\SettingController');
    Route::resource('/categories', 'admin\CategoriesController');
    Route::resource('/sliders', 'admin\SlidersController');
    Route::resource('/stores', 'admin\StoresController');
    Route::resource('/coupons', 'admin\CouponsController');
    Route::resource('/orders', 'admin\OrdersController');
    Route::resource('/offers', 'admin\OffersController');
    Route::resource('/invoices', 'admin\InvoicesController');
});




Route::get('mandoub/{user_id?}', 'UsersController@mandoub')->name('mandoub');
Route::post('signupDelegation', 'UsersController@signupDelegation')->name('signupDelegation');

Route::get('signup-succeed', 'UsersController@signupSucceed')->name('signupSucceed');
